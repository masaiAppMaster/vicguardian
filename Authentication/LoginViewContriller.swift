//
//  LoginViewContriller.swift
//  VicGuardian
//
//  Created by sai ma on 2/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation
import Firebase

class LoginViewController: UIViewController {
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    var swiftyOnboard: SwiftyOnboard!
    
    
    @IBOutlet weak var registerLink: UILabel!
    
    var app: App!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        app = getAppDelegate().app
        addTapToHideKeyboardGesture()
        if UserDefaults.standard.value(forKey: "hasSignin") == nil {
            try? Auth.auth().signOut()
        }
        Auth.auth().addStateDidChangeListener { auth, user in
            if let user = user {
                self.app.login(uid: user.uid)
                self.performSegue(withIdentifier: "goToIndex", sender: nil)
            } else {
                // No user is signed in.
            }
        }
        swiftyOnboard = SwiftyOnboard(frame: view.frame)
        swiftyOnboard.backgroundColor = UIColor.white
        view.addSubview(swiftyOnboard)
        swiftyOnboard.dataSource = self
        
        registerLink.isUserInteractionEnabled = true
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(labelTapped))
        registerLink.addGestureRecognizer(gestureRecognizer)
        registerLink.attributedText = NSAttributedString(string: "Do not have an account? Sign up now!", attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue])
    }
    
    @objc func labelTapped(){
        self.performSegue(withIdentifier: "registerViewSegue", sender: self)
    }
    
    @IBAction func login(_ sender: Any) {
        guard let email = self.email.text, let password = self.password.text else {
            return
        }
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            UserDefaults.standard.set(true, forKey: "hasSignin")
            if let error = error {
                let alert = UIAlertController(title: "Error", message: "User Or Password is incorrect.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                    NSLog("The \"OK\" alert occured.")
                }))
                self.present(alert, animated: true, completion: nil)
                print("log in failed\(error)")
            } else {
                self.app.login(uid: authResult!.user.uid)
                self.performSegue(withIdentifier: "goToIndex", sender: nil)
            }
        }
    }
    
    func addTapToHideKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(tap:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(tap: UITapGestureRecognizer) {
        self.email.endEditing(true)
        self.password.endEditing(true)
    }
    
    @objc func handleSkip() {
        swiftyOnboard?.removeFromSuperview()
        //        SwiftyOnboard?.goToPage(index: 2, animated: true)
    }
    
    @objc func handleContinue(sender: UIButton) {
        swiftyOnboard?.removeFromSuperview()
//        SwiftyOnboard?.goToPage(index: index + 1, animated: true)
    }
}

extension LoginViewController: SwiftyOnboardDataSource {
    
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: SwiftyOnboard) -> Int {
        return 5
    }
    
    func swiftyOnboardViewForOverlay(_ swiftyOnboard: SwiftyOnboard) -> SwiftyOnboardOverlay? {
        let overlay = SwiftyOnboardOverlay()
        
        //Setup targets for the buttons on the overlay view:
        overlay.skipButton.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
        overlay.continueButton.addTarget(self, action: #selector(handleContinue), for: .touchUpInside)
        
        //Setup for the overlay buttons:
        overlay.continueButton.titleLabel?.font = UIFont(name: "Lato-Bold", size: 16)
        overlay.continueButton.setTitleColor(UIColor.white, for: .normal)
        overlay.skipButton.setTitleColor(UIColor.white, for: .normal)
        overlay.skipButton.titleLabel?.font = UIFont(name: "Lato-Heavy", size: 16)
        overlay.continueButton.isHidden = true
        //Return the overlay view:
        return overlay
    }

    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: SwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        let page = SwiftyOnboardPage()
        page.imageView.image = UIImage(named: "\(index+1).jpg")
        
        return page
    }
    
//    func swiftyOnboardViewForBackground(swiftyOnboard: SwiftyOnboard) -> UIView? {
//        switch (swiftyOnboard.currentPage) {
//        case 0: return UIImage()
//        case 1: return UIView()
//        case 2: return UIView()
//        default:
//            return nil
//        }
//    }

}
