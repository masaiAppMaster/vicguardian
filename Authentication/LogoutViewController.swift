//
//  LogoutViewController.swift
//  VicGuardian
//
//  Created by sai ma on 12/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import UIKit
import Firebase

class LogoutViewController: UIViewController {
    var userDataManager: UserDataManager!
    var locationManager: LocationManager!
    var cancelButtonHidden: Bool = true
    var meRef: DatabaseReference!
    var protectorRef: DatabaseReference!
    var testRef: DatabaseReference!
    var subscription: Subscription<UserDataEvent>?
    
    @IBOutlet weak var AccountEmailLabel: UILabel!
    @IBOutlet weak var helperEmialLabel: UILabel!
    
    @IBOutlet weak var protectorEmialTextfeild: UITextField!
    
    @IBAction func logoutButton(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        
        do {
            UserDefaults.standard.set(false, forKey: "hasSignin")
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        locationManager.stopUpdata()
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "login")
        self.present(newViewController, animated: true, completion: nil)
    }
    
    @IBAction func editButton(_ sender: Any) {
       
        let refreshAlert = UIAlertController(title: "Change Protector", message: "Do you want to change the protector", preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            self.cancelButton.isHidden = false
            self.protectorEmialTextfeild.isUserInteractionEnabled = true
            self.protectorEmialTextfeild.text = ""
            self.protectorEmialTextfeild.placeholder = "Input VicGuardian Account"
            self.protectorEmialTextfeild.borderStyle = UITextField.BorderStyle(rawValue: 3)!
            self.meRef.removeValue()
            self.protectorRef.removeValue()
            self.addTapToHideKeyboardGesture()
            
        }))
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBAction func confrimButtonAction(_ sender: Any) {
        
        if protectorEmialTextfeild.text == nil{
            wrongEmail()
        } else {
            checkHasEmail()
            
            if protectID == nil {
                noUserAlert()
            }else {
                if protectID == userDataManager.helperID{
                    sameProtecteeIDNotification()
                }else {
                    findProtectorID()
                    userDataManager = getAppDelegate().app.userDataManager
                    singleUserRealtionship.hashelper ? fullHelperAlert() : addUsersRelationship()
                }
            }
        }
        
    }
    
    
    func sameProtecteeIDNotification() {
        let alert = UIAlertController(title: "Notification", message: "This user has been your protectee, please enter new user, Or contact this user to delete your protect relationship ", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userDataManager = getAppDelegate().app.userDataManager
        locationManager = getAppDelegate().app.locationManager
        protectorEmialTextfeild.text = self.userDataManager.emergencyName
        protectorEmialTextfeild.isUserInteractionEnabled = false
        self.protectorEmialTextfeild.borderStyle = UITextField.BorderStyle(rawValue: 0)!
        helperEmialLabel.text = self.userDataManager.helperName
        AccountEmailLabel.text = self.userDataManager.userName
        cancelButton.isHidden = cancelButtonHidden
        meRef = Database.database().reference().child("relationship").child(userDataManager.uid).child("emengecyID")
        protectorRef = Database.database().reference().child("relationship").child(userDataManager.emergencyID ?? "0").child("helperID")
        listenHelper()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        cancelButton.isHidden = true
        if userDataManager.hasEmergency {
            protectorEmialTextfeild.text = self.userDataManager.emergencyName
        }else {
            protectorEmialTextfeild.text = ""
        }
        
        protectorEmialTextfeild.isUserInteractionEnabled = false
        self.protectorEmialTextfeild.borderStyle = UITextField.BorderStyle(rawValue: 0)!
        self.protectorEmialTextfeild.placeholder = ""
    }
    
    func listenHelper() {
        subscription = userDataManager.userDataEventDispatcher.subscribeOnMain() { event in
            switch event {
            case .helperIDUpdate:
                if self.userDataManager.hasHelp{
                self.helperEmialLabel.text = self.userDataManager.helperName
                }else{
                    self.helperEmialLabel.text = ""
                }
            
            case .emergencyMessageUpdate: break
            case .helperMessageUpdate: break
            }
        }
    }
    
    deinit {
        subscription?.unsubscribe()
    }
    
    func wrongEmail() {
        let alert = UIAlertController(title: "Error", message: "Please input email", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    var hasRelationship = false
    var protectID: String? = nil
    var singleUserRealtionship = Relationship()
    
    func fullHelperAlert() {
        let alert = UIAlertController(title: "Error", message: "Cann't add this user and this user has helper", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func addUsersRelationship() {
//        userDataManager = getAppDelegate().app.userDataManager
        let addRef = Database.database().reference().child("relationship").child(self.protectID!)
        singleUserRealtionship.HelperID = userDataManager.uid
        addRef.setValue([
            "emengecyID" : singleUserRealtionship.emengecyID,
            "helperID" : userDataManager.uid,
            "myID" : singleUserRealtionship.myID
            ])
        let addMeRef = Database.database().reference().child("relationship").child(userDataManager.uid)
        addMeRef.setValue([
            "emengecyID" : protectID,
            "helperID" : userDataManager.helperID,
            "myID" : userDataManager.uid
            ])
        //        performSegue(withIdentifier: "goToMother", sender: nil)
//        self.navigationController?.popViewController(animated: true)
        cancelButton.isHidden = true
        protectorEmialTextfeild.text = self.userDataManager.emergencyName
        protectorEmialTextfeild.isUserInteractionEnabled = false
        self.protectorEmialTextfeild.borderStyle = UITextField.BorderStyle(rawValue: 0)!
        
    }
    
    func checkHasEmail() {
        for user in userDataManager.users {
            guard let protectorEmialTextfeild = self.protectorEmialTextfeild.text else { return }
            hasRelationship = false
            user.email == protectorEmialTextfeild ? self.hasRelationship = true : nil
            hasRelationship ? self.protectID = user.id : nil
        }
    }
    
    func noUserAlert() {
        let alert = UIAlertController(title: "Error", message: "Incorrect user", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func findProtectorID() {
        for singleRaktionship in userDataManager.relationships {
            singleRaktionship.myID == self.protectID! ? self.singleUserRealtionship = singleRaktionship : nil
            
        }
    }
    
    @IBAction func testButton(_ sender: Any) {
        testRef = Database.database().reference().child("relationship").child("yOak4vCIXJhnVdDTEx42HA3GLG3")
        testRef.setValue([
            "test" : "test"
            ])
    }
    
    //
//    func showAlert() {
//        let refreshAlert = UIAlertController(title: "Refresh", message: "All data will be lost.", preferredStyle: UIAlertController.Style.alert)
//
//        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
//            print("Handle Ok logic here")
//        }))
//
//        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
//            print("Handle Cancel Logic here")
//        }))
//
//        present(refreshAlert, animated: true, completion: nil)
//    }
//

    
//    func addTapToHideKeyboardGesture() {
//        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(tap:)))
//        self.view.addGestureRecognizer(tap)
//    }
//
//    @objc func dismissKeyboard(tap: UITapGestureRecognizer) {
////        self.email.endEditing(true)
////        self.password.endEditing(true)
//        self.AccountEmailLabel.endEditing(true)
//    }
    func addTapToHideKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(tap:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(tap: UITapGestureRecognizer) {
        self.protectorEmialTextfeild.endEditing(true)
    }
}
