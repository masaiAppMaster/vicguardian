//
//  RegisterViewController.swift
//  wechat
//
//  Created by sai ma on 29/1/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController {
    
    var ref: DatabaseReference!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var verify: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var phone: UITextField!
    var dataManager: UserDataManager!
    
    @IBAction func register(_ sender: Any) {
        //TODO: verify
        
        
        guard let email = self.email.text, let password = self.password.text, let name = self.verify.text, let phone = self.phone.text else {
            return
        }
        
        
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            guard let user = authResult?.user else {
                let alert = UIAlertController(title: "Error", message: "\(String(describing: error!.localizedDescription))", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                    NSLog("The \"OK\" alert occured.")
                }))
                self.present(alert, animated: true, completion: nil)
//                self.errorLabel.text = error?.localizedDescription
//                self.errorLabel.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
                return
            }
            
            getAppDelegate().app.login(uid: user.uid)
            print("regiseter success\(user)")
            let ref = Database.database().reference().child("user").childByAutoId()
//            UserDefaults.standard.set(fcmToken, forKey: "token")
            let userToken = UserDefaults.standard.string(forKey: "token")
            ref.setValue([
                
                "token" : userToken ?? "",
                "email" : email,
                "id" : Auth.auth().currentUser!.uid,
                "name" : name,
                "phone" : phone
                ])
            self.performSegue(withIdentifier: "goToLogin", sender: nil)
            //传入数据库
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapToHideKeyboardGesture()
        dataManager = getAppDelegate().app.userDataManager
        
    }
    
    func addTapToHideKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(tap:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(tap: UITapGestureRecognizer) {
//        self.email.endEditing(true)
//        self.password.endEditing(true)
        self.email.endEditing(true)
        self.verify.endEditing(true)
        self.password.endEditing(true)
        self.phone.endEditing(true)
        
    }
}
