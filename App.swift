//
//  App.swift
//  VicGuardian
//
//  Created by sai ma on 2/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation

class App {
    
    var userDataManager: UserDataManager!
    var criminalDataManager: CriminalDataManager!
    var locationManager: LocationManager!
    
    func login(uid: String) {
        userDataManager = UserDataManager(uid: uid)
        criminalDataManager = CriminalDataManager()
        locationManager = LocationManager()
        locationManager.start(userid: uid)
    }
}

