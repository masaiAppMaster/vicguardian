//
//  LocationAnnotation.swift
//  VicGuardian
//
//  Created by erica12580 on 17/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation
import MapKit

class LocationAnnotation: NSObject, MKAnnotation{
    
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var image: UIImage? = nil
    
    @objc init( newTitle: String, newSubtitle: String, lat: Double, long: Double){
        title = newTitle
        subtitle = newSubtitle
        coordinate = CLLocationCoordinate2D()
        coordinate.latitude = lat
        coordinate.longitude = long
    }
    
}
