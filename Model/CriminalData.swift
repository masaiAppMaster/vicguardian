//
//  CriminalData.swift
//  VicGuardian
//
//  Created by sai ma on 7/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation
/* {
 "Year": 2012,
 "Suburb": "ABBOTSFORD",
 "Kind": "B Property and deception offences",
 "Count": 530
 }*/

struct CriminalData: Decodable {
    let year: Int
    let suburb: String
    let kind: String
    let count: Int
    
    
    enum CodingKeys: String, CodingKey {
        case year = "Year"
        case suburb = "Suburb"
        case kind = "Kind"
        case count = "Count"
    }
}


class CriminalSingleData {
    var kind = ""
    var count = 0
    enum CodingKeys: String, CodingKey {
        case kind = "Kind"
        case count = "Count"
    }
}



class CriminalSuburbYearCountData {
    var year = 0
    var count = 0
}


