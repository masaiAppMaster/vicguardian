//
//  TimerManager.swift
//  VicGuardian
//
//  Created by sai ma on 17/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import UIKit

class TimerManager: NSObject {
    
    static weak var timer: Timer?
    static var seconds = 0
    static var pauseSeconds = 0
    static var pauseBool = false
    static var timeString: ((_ value: String?, _ ends: Bool) -> Void)?
    
    static func start(withSeconds: Int) -> Void {
        timer?.invalidate()
        seconds = withSeconds
        timer = Timer.scheduledTimer(timeInterval: 1, target:self, selector: #selector(TimerManager.update), userInfo: nil, repeats: true)
    }
    
    @objc static func update() -> Void {
        if seconds == 0 {
            timer?.invalidate()
            timeString?(nil,true)
        } else {
            seconds -= 1
            let value = getString(from: TimeInterval(seconds))
            
            timeString?(value,false)
            
        }
    }
    
    static private func getString(from time: TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format: "%02i:%02i:%02i", hours,minutes,seconds)
    }
    
    static func destroy() -> Void {
        seconds = 0
        timer?.invalidate()
        timer = nil
    }
    
    static func pause() {
//        pauseSeconds = seconds
        pauseBool = true
        pauseSeconds = seconds
        seconds = 0
        timer!.invalidate()
        destroy()
    }
    
    static func continuePasue() {
        if pauseSeconds != 0 {
        start(withSeconds: Int(pauseSeconds))
        }
        pauseBool = false
    }
}
