//
//  User.swift
//  VicGuardian
//
//  Created by sai ma on 2/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation

class User {
    var email = ""
    var id = ""
    var name = ""
    var phone = ""
    var hashelper = false
}
