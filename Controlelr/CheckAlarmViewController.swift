//
//  checkAlarmViewController.swift
//  VicGuardian
//
//  Created by sai ma on 2/5/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation
import UIKit

class CheckAlarmViewController: UIViewController {
    var userDataManager: UserDataManager!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDataManager = getAppDelegate().app.userDataManager
        
        if userDataManager.hasEmergency{
            if TimerManager.seconds != 0 {
                self.performSegue(withIdentifier: "showTimeSegue", sender: nil)
            }else{
                self.performSegue(withIdentifier: "setTimeAlarmSegue", sender: nil)
            }
            
            }else{
                self.performSegue(withIdentifier: "alarmAddRealtionshipSegue", sender: nil)
                }
    }
}

