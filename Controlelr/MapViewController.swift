
import Foundation
import UIKit
import MapKit
import Firebase
import CoreLocation
import GooglePlaces
import AVFoundation
import MediaPlayer

protocol HandleMapSearch: class {
    func dropPinZoomIn(placemark:MKPlacemark)
}

enum MapDataEvent {
    case currentLocationUpdate
}

class MapViewController: UIViewController, LocationListViewControllerDelegate, CLLocationManagerDelegate, HandleMapSearch, MKMapViewDelegate, RecommendListViewControllerDelegate, SafePlaceSelectorViewControllerDelegate {
    
    public let listDataEventDispatcher = EventDispatcher<MapDataEvent>()
    var subscription: Subscription<MapDataEvent>?
    
    @IBOutlet weak var mapView: MKMapView!
    
    var policeDataRef: DatabaseReference?
    var petrolStationDataRef: DatabaseReference?
    var shopDataRef: DatabaseReference?
    var railwayDataRef: DatabaseReference?
    var userDataManager: UserDataManager!
    var meRef: DatabaseReference!
    var pinAnnotationView:MKAnnotationView!
    var player: AVAudioPlayer?
    var formattedAddress: String?
    var playing: Bool = false
    var flashing: Bool = false
    var currentExists: Bool = false
    var opening: Bool = true
    var policeChecked: Bool = true
    var shopChecked: Bool = true
    var gasChecked: Bool = true
    var railwayChecked: Bool = true
    var preferenceExists: Bool = false
    var turnedOn: Bool = false
    
    var locationManager: CLLocationManager = CLLocationManager()
    var currentLocation: CLLocationCoordinate2D?
    // Maintain the current selected center to search for nearby safe places.
    var selectedCurrentCenter: CLLocationCoordinate2D?
    var selectedPin: MKPlacemark?
    var selectedAnnotation: MKAnnotation?
    var resultSearchController: UISearchController!
    var nearbySafePlace: [LocationAnnotation]? = []
    var filteredNearbySafePlace: [LocationAnnotation] = []
    let dispatchGroup = DispatchGroup()
    var currentlocationManager: LocationManager!
    
    override func viewDidLoad() {
        
        let tabVC = self.parent?.parent as? UITabBarController
        tabVC?.viewControllers?.forEach{_ = $0.view}
        
        
        super.viewDidLoad()
        self.view.showLoadingIndicator()
        userDataManager = getAppDelegate().app.userDataManager
        //        placesClient = GMSPlacesClient.shared()
        
        //        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        //        locationManager.distanceFilter = 10
        
        currentlocationManager = getAppDelegate().app.locationManager
        
        //        currentlocationManager.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let status = CLLocationManager.authorizationStatus()
        
        // Check for autorization status
        switch status {
        case .notDetermined:
            currentlocationManager.locationManager.requestWhenInUseAuthorization()
            return
            
        case .denied, .restricted:
            let alert = UIAlertController(title: "Location Services disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            
            present(alert, animated: true, completion: nil)
            return
        case .authorizedAlways, .authorizedWhenInUse:
            break
        }
        
        currentLocation = currentlocationManager.currentLocation
        selectedCurrentCenter = currentlocationManager.currentLocation
        
//        currentlocationManager.locationManager.delegate = self
//        currentlocationManager.locationManager.startUpdatingLocation()
        
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        currentLocation = locationManager.location?.coordinate
        selectedCurrentCenter = locationManager.location?.coordinate
        
        

        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController.searchResultsUpdater = locationSearchTable
        let searchBar = resultSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Search for places"
        navigationItem.titleView = resultSearchController?.searchBar
        resultSearchController.hidesNavigationBarDuringPresentation = false
        resultSearchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        locationSearchTable.mapView = mapView
        locationSearchTable.handleMapSearchDelegate = self
        mapView.delegate = self
        
        mapView.removeAnnotations(mapView.annotations)
        setDefaultPreference()
        
        checkPreferences()
        
        loadSafePlaceData()
        let locAn = LocationAnnotation(newTitle: "Current Location", newSubtitle: "", lat: currentLocation!.latitude, long: currentLocation!.longitude)
        self.focusOn(annotation: locAn)
    }
    
    func setDefaultPreference() {
        let ref = Database.database().reference().child("userPreferences").child(userDataManager.uid)
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if !snapshot.exists(){
                ref.setValue([
                    "policeChecked": true,
                    "shopChecked": true,
                    "gasChecked": true,
                    "railwayChecked": true
                    ])
            }
        })
    }
    
    deinit {
        subscription?.unsubscribe()
    }
    
    func listen() {
        subscription = listDataEventDispatcher.subscribeOnMain() { event in
            switch event {
            case .currentLocationUpdate:
                let locAn = LocationAnnotation(newTitle: "Current Location", newSubtitle: "", lat: self.currentLocation!.latitude, long: self.currentLocation!.longitude)
                self.focusOn(annotation: locAn)
                print("updated.")
            }
        }
    }
    
    // Check for user's settings of preferences
    func checkPreferences() {
        let ref = Database.database().reference().child("userPreferences").child(userDataManager.uid)
        ref.observe(DataEventType.value) { snapShot in
            if let dict = snapShot.value as? NSDictionary {
                self.policeChecked = dict["policeChecked"] as! Bool
                self.shopChecked = dict["shopChecked"] as! Bool
                self.gasChecked = dict["gasChecked"] as! Bool
                self.railwayChecked = dict["railwayChecked"] as! Bool
            }
            self.view.hideLoadingIndicator()
        }
    }
    
    func displayIntroduction() {
        let alert = UIAlertController(title: "Tips", message: "Red pins show the safe places around you.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations:
        [CLLocation])
    {
        //        let location: CLLocation = locations.last!
        listen()
        let location: CLLocation = manager.location!
        if currentLocation == nil {
            currentLocation = CLLocationCoordinate2DMake(-37.877512, 145.044216)
            selectedCurrentCenter = currentLocation
        }
        else {
            currentLocation = location.coordinate
            selectedCurrentCenter = location.coordinate
        }
        let locAn = LocationAnnotation(newTitle: "Current Location", newSubtitle: "", lat: currentLocation!.latitude, long: currentLocation!.longitude)
        //        self.focusOn(annotation: locAn)
        
        mapView.showsUserLocation = true
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: NSError) {
        print(error.localizedDescription)
    }
    
    // Handles safe place selector action
    func didSaveSelector() {
        mapView.removeAnnotations(mapView.annotations)
        loadSafePlaceData()
    }
    
    // Show the search result and search for nearby safe places.
    func dropPinZoomIn(placemark: MKPlacemark){
        // cache the pin
        selectedPin = placemark
        selectedCurrentCenter = placemark.coordinate
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        
        if let city = placemark.locality,
            let state = placemark.administrativeArea {
            annotation.subtitle = "\(city) \(state)"
        }
        
        mapView.addAnnotation(annotation)
        let span = MKCoordinateSpan(latitudeDelta: 0.03, longitudeDelta: 0.03)
        let region = MKCoordinateRegion(center: placemark.coordinate, span: span)
        mapView.setRegion(region, animated: true)
        mapView.selectAnnotation(annotation, animated: true)
        loadSafePlaceData()
    }
    
    // Handle choosed locaiton from saved list.
    func didSelectLocation(_ annotation: LocationAnnotation) {
        mapView.removeAnnotations(mapView.annotations)
        selectedCurrentCenter = annotation.coordinate
        mapView.addAnnotation(annotation)
        loadSafePlaceData()
        self.focusOn(annotation: annotation)
    }
    
    // Handle choosed location from recommend list.
    func didSelectSafePlace(_ annotation: LocationAnnotation) {
        selectedCurrentCenter = annotation.coordinate
        mapView.addAnnotation(annotation)
        loadSafePlaceData()
        self.focusOn(annotation: annotation)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func addAnnotation(annotation: LocationAnnotation) {
        mapView.addAnnotation(annotation)
    }
    
    func focusOn(annotation: LocationAnnotation) {
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegion.init(center: annotation.coordinate,
                                                       latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
        mapView.selectAnnotation(annotation, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "locationListSegue"{
            let controller = segue.destination as! LocationListViewController
            controller.delegate = self
        }
        if segue.identifier == "selectorSegue"{
            let controller = segue.destination as! SafePlaceSelectorViewController
            controller.delegate = self
        }
        if segue.identifier == "detailViewSegue" {
            if let destinationVC = segue.destination as? detailViewController {
                destinationVC.passedSelectedAnnotation = mapView.selectedAnnotations.first
            }
        }
        if segue.identifier == "recommendListSegue"{
            
            if let destinationVC = segue.destination as? RecommendListViewController {
                
                //                for locAn in nearbySafePlace!.dropLast() {
                //                    getOpenningInfo(location: locAn)
                //                }
                
                destinationVC.passedAnnotation = self.filteredNearbySafePlace
                
                //                checkOpeningStatus(locationArray: nearbySafePlace!.dropLast())
                //                for locAn in nearbySafePlace!.dropLast() {
                //                    var index = 0
                //                    getGoogleAdrress(location: locAn)
                //                    if !opening {
                //                        nearbySafePlace?.remove(at: index)
                //                    }
                //                    index += 1
                //                }
                
                
            }
        }
    }
    
    
    func checkOpeningStatus(locationArray: [LocationAnnotation]) {
        for locAn in locationArray {
            getOpenningInfo(location: locAn)
        }
        
    }
    
    // Load data of safe places from firebase.
    func loadSafePlaceData(){
        // Check if selceted pin is the current location
        if selectedCurrentCenter?.latitude == currentLocation?.latitude && selectedCurrentCenter?.longitude == currentLocation?.longitude {
            nearbySafePlace = []
            currentExists = false
        }
        getPoliceData()
        get24HourShopData()
        getPetrolStationData()
        getRailwayData()
        
        //        dispatchGroup.notify(queue: .main) {
        //            self.checkOpeningStatus(locationArray: self.nearbySafePlace!)
        //        }
        
    }
    
    func getPoliceData(){
        policeDataRef = Database.database().reference().child("policeData")
        policeDataRef?.observe(DataEventType.value) { snapShot in
            let enumerator = snapShot.children
            var array: [LocationAnnotation] = []
            while let snapShotValue = enumerator.nextObject() as? DataSnapshot {
                let dict = snapShotValue.value as? NSDictionary
                let lat = dict?["lat"] as! Double
                let long = dict?["long"] as! Double
                let title = dict?["name"] as! String
                let locAn = LocationAnnotation(newTitle: title, newSubtitle: "Police Station", lat: lat, long: long)
                locAn.image = UIImage(named: "Police_2")
                array.append(locAn)
                
            }
            if self.policeChecked {
                self.showNearByData(locArray: array)
            }
            
        }
    }
    
    func getRailwayData(){
        railwayDataRef = Database.database().reference().child("railwayData")
        railwayDataRef?.observe(DataEventType.value) { snapShot in
            let enumerator = snapShot.children
            var array: [LocationAnnotation] = []
            while let snapShotValue = enumerator.nextObject() as? DataSnapshot {
                let dict = snapShotValue.value as? NSDictionary
                let lat = dict?["lat"] as! Double
                let long = dict?["long"] as! Double
                var title = dict?["name"] as! String
                if !title.contains("RAILWAY STATION") {
                    title += " RAILWAY STATION"
                }
                let locAn = LocationAnnotation(newTitle: title, newSubtitle: "Railway Station", lat: lat, long: long)
                locAn.image = UIImage(named: "Train")
                array.append(locAn)
            }
            if self.railwayChecked {
                self.showNearByData(locArray: array)
            }
        }
    }
    
    func getPetrolStationData(){
        petrolStationDataRef = Database.database().reference().child("petrolStationData")
        petrolStationDataRef?.observe(DataEventType.value) { snapShot in
            let enumerator = snapShot.children
            var array: [LocationAnnotation] = []
            while let snapShotValue = enumerator.nextObject() as? DataSnapshot {
                let dict = snapShotValue.value as? NSDictionary
                let lat = dict?["LATITUDE"] as! Double
                let long = dict?["LONGITUDE"] as! Double
                let title = dict?["NAME"] as! String
                let locAn = LocationAnnotation(newTitle: title, newSubtitle: "Gas Station", lat: lat, long: long)
                locAn.image = UIImage(named: "Gas_2")
                array.append(locAn)
            }
            if self.gasChecked {
                self.showNearByData(locArray: array)
            }
        }
    }
    
    func get24HourShopData(){
        shopDataRef = Database.database().reference().child("24HourShopData")
        shopDataRef?.observe(DataEventType.value) { snapShot in
            let enumerator = snapShot.children
            var array: [LocationAnnotation] = []
            while let snapShotValue = enumerator.nextObject() as? DataSnapshot {
                let dict = snapShotValue.value as? NSDictionary
                let lat = dict?["Latitude"] as! Double
                let long = dict?["Longitude"] as! Double
                let title = dict?["Name"] as! String
                let locAn = LocationAnnotation(newTitle: title, newSubtitle: "24 Hour Shop", lat: lat, long: long)
                locAn.image = UIImage(named: "Shop_2")
                array.append(locAn)
            }
            if self.shopChecked {
                self.showNearByData(locArray: array)
            }
        }
    }
    
    // Get the place annotation array, search for nearby ones acoording to distance from current center and show it on map.
    func showNearByData(locArray: [LocationAnnotation]){
        if currentLocation == nil {
            currentLocation = CLLocationCoordinate2DMake(-37.877512, 145.044216)
            selectedCurrentCenter = currentLocation
            let span = MKCoordinateSpan(latitudeDelta: 0.03, longitudeDelta: 0.03)
            let region = MKCoordinateRegion(center: currentLocation!, span: span)
            mapView.setRegion(region, animated: true)
        }
        //        let request = MKDirections.Request()
        //        let source = MKPlacemark(coordinate: selectedCurrentCenter!)
        
        
        let loc1 = CLLocation(latitude: selectedCurrentCenter!.latitude, longitude: selectedCurrentCenter!.longitude)
        for locAn in locArray{
            let loc2 = CLLocation(latitude: locAn.coordinate.latitude, longitude: locAn.coordinate.longitude)
            let distance = loc1.distance(from: loc2)
            
            //            let desP = CLLocationCoordinate2DMake(locAn.coordinate.latitude, locAn.coordinate.longitude)
            //            let destination = MKPlacemark(coordinate: desP)
            //            request.source = MKMapItem(placemark: source)
            //            request.destination = MKMapItem(placemark: destination)
            //            request.transportType = MKDirectionsTransportType.walking
            //            request.requestsAlternateRoutes = true
            //            let directions = MKDirections(request: request)
            //            directions.calculate { (response, error) in
            //                if let response = response, let route = response.routes.first {
            //                    if route.distance < 2000 {
            //                        self.mapView.addAnnotation(locAn)
            //                    }
            //                }
            //            }
            if distance < 2000{
                // Check if selceted pin is the current location
                if selectedCurrentCenter?.latitude == currentLocation?.latitude && selectedCurrentCenter?.longitude == currentLocation?.longitude {
                    nearbySafePlace?.append(locAn)
                }
                mapView.addAnnotation(locAn)
            }
        }
        
    }
    
    // Filter the locations according to checked location type
    func filterWithType(locArray: [LocationAnnotation]) -> [LocationAnnotation]{
        var newArray: [LocationAnnotation] = []
        for locAn in locArray {
            if locAn.subtitle == "Police Station" && policeChecked {
                newArray.append(locAn)
            }
            if locAn.subtitle == "Gas Station" && gasChecked {
                newArray.append(locAn)
            }
            if locAn.subtitle == "24 Hour Shop" && shopChecked {
                newArray.append(locAn)
            }
            if locAn.subtitle == "Railway Station" && railwayChecked {
                newArray.append(locAn)
            }
        }
        return newArray
    }
    
    
    @IBAction func showCurrentLocation(_ sender: Any) {
        if let currentAn = currentLocation {
            let locAn = LocationAnnotation(newTitle: "Current Location", newSubtitle: "", lat: currentAn.latitude, long: currentAn.longitude)
            selectedCurrentCenter = currentLocation
            mapView.removeAnnotations(mapView.annotations)
            loadSafePlaceData()
            self.focusOn(annotation: locAn)
        }
    }
    
    // Customize pin view for different annotations.
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard !(annotation is MKUserLocation) else { return nil }
        
        // Check if it is user's saved places or safe places.
        if annotation is LocationAnnotation {
            let reuseId = "original"
            var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
            if pinView == nil {
                pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            }
            pinView?.canShowCallout = true
            let locAn = annotation as? LocationAnnotation
            var pinImage = locAn?.image
            
            // If the annotation is from saved places, check the type of the place.
            if pinImage == nil {
                if locAn?.subtitle == "Police Station" {
                    pinImage = UIImage(named: "Police_2")
                } else if locAn?.subtitle == "Gas Station" {
                    pinImage = UIImage(named: "Gas_2")
                } else if locAn?.subtitle == "24 Hour Shop" {
                    pinImage = UIImage(named: "Shop_2")
                } else if locAn?.subtitle == "Railway Station"{
                    pinImage = UIImage(named: "Train")
                }
                else {
                    pinImage = UIImage(named: "Star")
                }
            }
            let size = CGSize(width: 40, height: 40)
            UIGraphicsBeginImageContext(size)
            pinImage!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            pinView?.image = resizedImage
            
            let smallSquare = CGSize(width: 35, height: 35)
            let leftButton = UIButton(frame: CGRect(origin: .zero, size: smallSquare))
            leftButton.setBackgroundImage(UIImage(named: "Navi"), for: [])
            leftButton.addTarget(self, action: #selector(MapViewController.getDirectionsOriginal), for: .touchUpInside)
            pinView?.leftCalloutAccessoryView = leftButton
            
            //            let rightButton = UIButton(type: .detailDisclosure)
            //            rightButton.frame.size = smallSquare
            let rightButton = UIButton(frame: CGRect(origin: .zero, size: smallSquare))
            rightButton.setBackgroundImage(UIImage(named: "Info"), for: [])
            rightButton.addTarget(self, action: #selector(MapViewController.getDetails), for: .touchUpInside)
            pinView?.rightCalloutAccessoryView = rightButton
            
            
            return pinView
        }
        else if annotation is MKPointAnnotation {
            // These are user search results.
            let reuseId = "pin"
            var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
            if pinView == nil {
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            }
            pinView?.annotation = annotation
            pinView?.pinTintColor = UIColor.red
            pinView?.canShowCallout = true
            let smallSquare = CGSize(width: 30, height: 30)
            let leftButton = UIButton(frame: CGRect(origin: .zero, size: smallSquare))
            leftButton.setBackgroundImage(UIImage(named: "Navi"), for: [])
            leftButton.addTarget(self, action: #selector(MapViewController.getDirections), for: .touchUpInside)
            pinView?.leftCalloutAccessoryView = leftButton
            
            let rightButton = UIButton(frame: CGRect(origin: .zero, size: smallSquare))
            rightButton.setBackgroundImage(UIImage(named: "Info"), for: [])
            rightButton.addTarget(self, action: #selector(MapViewController.getDetails), for: .touchUpInside)
            pinView?.rightCalloutAccessoryView = rightButton
            
            return pinView
        }
        else { return nil }
    }
    
    // Get directions to user's search result.
    @objc func getDirections(){
        guard let selectedPin = selectedPin else { return }
        let mapItem = MKMapItem(placemark: selectedPin)
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeWalking]
        mapItem.openInMaps(launchOptions: launchOptions)
    }
    
    // Get directions to safe places.
    @objc func getDirectionsOriginal(){
        let annotation = mapView.selectedAnnotations.first
        let selectedAnnotation = MKPlacemark.init(coordinate: annotation!.coordinate)
        let mapItem = MKMapItem(placemark: selectedAnnotation)
        mapItem.name = annotation?.title!
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeWalking]
        mapItem.openInMaps(launchOptions: launchOptions)
    }
    
    @objc func getDetails(){
        self.performSegue(withIdentifier: "detailViewSegue", sender: self)
        //        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //        let newViewController = storyBoard.instantiateViewController(withIdentifier: "newLocationViewController") as! NewLocationViewController
        //        newViewController.passedSelectedAnnotation = mapView.selectedAnnotations.first
        //        self.present(newViewController, animated: true, completion: nil)
    }
    
    func send(text: String) {
        // Check whether user has a protector
        if userDataManager.hasEmergency {
            meRef = Database.database().reference().child("message").child(userDataManager.uid).child(userDataManager.emergencyID!)
            let date = Date()
            let timeInterval = date.timeIntervalSince1970
            let timestamp = timeInterval.description.replacingOccurrences(of: ".", with: "").padding(toLength: 16, withPad: "0", startingAt: 0)
            let childRef = meRef.childByAutoId()
            childRef.setValue([
                "text" : text,
                "type" : "text",
                "timestamp": timestamp
                ])
        }
    }
    
    // Get address from latitude and longitude.
    func getAddressInfo(location: MKAnnotation) {
        var lookUpStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(location.coordinate.latitude),\(location.coordinate.longitude)&key=AIzaSyCniR4gRiVeDqK8NG6gGQnAiQ5JH2N9wvI"
        
        lookUpStr = lookUpStr.replacingOccurrences(of: " ", with: "+")
        
        var jsonDict: [AnyHashable : Any]? = nil
        let url = URL(string: lookUpStr)
        let session = URLSession.shared
        if let usableUrl = url {
            let task = session.dataTask(with: usableUrl, completionHandler: { (data, response, error) in
                if let data = data {
                    do {
                        jsonDict = try JSONSerialization.jsonObject(with: data, options: []) as? [AnyHashable : Any]
                    } catch {
                        print(error)
                    }
                    
                    print(jsonDict)
                    let jsonResults = jsonDict?["results"] as? Array<Any>
                    let addressResult = jsonResults?.first as? Dictionary<String, Any>
                    self.formattedAddress = addressResult?["formatted_address"] as! String
                    
                    // Send message to protector.
                    var text = ""
                    text = "I feel unsafe! \nMy location: \nAddress: \(self.formattedAddress!) \nLatitude: \(location.coordinate.latitude) \nLongitude: \(location.coordinate.longitude)"
                    self.send(text: text)
                    self.sendImage()
                }
            })
            task.resume()
        }
    }
    
    func sendImage() {
        if userDataManager.hasEmergency {
            meRef = Database.database().reference().child("message").child(userDataManager.uid).child(userDataManager.emergencyID!)
            let date = Date()
            let timeInterval = date.timeIntervalSince1970
            let timestamp = timeInterval.description.replacingOccurrences(of: ".", with: "").padding(toLength: 16, withPad: "0", startingAt: 0)
            let childRef = meRef.childByAutoId()
            childRef.setValue([
                "image" : "Share Location for helpers",
                "type" : "image",
                "timestamp": timestamp
                ])
        }
        
    }
    
    @IBAction func callBtn(_ sender: Any) {
        guard let number = URL(string: "tel://" + "000000") else { return }
        UIApplication.shared.open(number)
    }
    
    
    @IBOutlet weak var unsafeBtn: UIButton!
    
    @IBAction func unsafeButton(_ sender: Any) {
        if userDataManager.hasEmergency{
            if !turnedOn {
                filteredNearbySafePlace = []
                if let currentAn = currentLocation {
                    let currentAnnotation = LocationAnnotation(newTitle: "Current Location", newSubtitle: "", lat: currentAn.latitude, long: currentAn.longitude)
                    if !currentExists {
                        nearbySafePlace?.append(currentAnnotation)
                        currentExists = true
                    }
                    
                    getAddressInfo(location: currentAnnotation)
                    //                var text = ""
                    //                text = "I feel unsafe! \nMy location: \nLatitude: \(currentAn.latitude) \nLongitude: \(currentAn.longitude)"
                    //                self.send(text: text)
                }
                
                if let nearbyPlace = nearbySafePlace {
                    for locAn in nearbyPlace.dropLast() {
                        getOpenningInfo(location: locAn)
                    }
                    // Perform segue when all tasks are done
                    dispatchGroup.notify(queue: .main) {
                        for locAn in nearbyPlace {
                            if locAn.subtitle == "Railway Station" {
                                self.filteredNearbySafePlace.append(locAn)
                            }
                        }
                        if let currentAn = self.currentLocation {
                            let currentAnnotation = LocationAnnotation(newTitle: "Current Location", newSubtitle: "", lat: currentAn.latitude, long: currentAn.longitude)
                            self.filteredNearbySafePlace.append(currentAnnotation)
                            self.performSegue(withIdentifier: "recommendListSegue", sender: self)
                        }
                    }
                }
                if !flashing {
                    turnFlash(self)
                }
                if !playing {
                    playSound(self)
                }
                turnedOn = true
                soundPlayButton.isEnabled = false
                flashButton.isEnabled = false
                unsafeBtn.setImage(UIImage(named: "Stop2.png"), for: .normal)
            } else {
                playSound(self)
                turnFlash(self)
                turnedOn = false
                soundPlayButton.isEnabled = true
                flashButton.isEnabled = true
                unsafeBtn.setImage(UIImage(named: "panic-btn.png"), for: .normal)
            }
        }else {
            let alert = UIAlertController(title: "notification", message: "Do not have protector", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func getOpenningInfo(location: MKAnnotation) {
        dispatchGroup.enter()
        var lookUpStr = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=\(location.title!!)&inputtype=textquery&fields=photos,formatted_address,name,opening_hours,permanently_closed&locationbias=circle:100@\(location.coordinate.latitude),\(location.coordinate.longitude)&key=AIzaSyCniR4gRiVeDqK8NG6gGQnAiQ5JH2N9wvI"
        
        lookUpStr = lookUpStr.replacingOccurrences(of: " ", with: "+")
        
        var jsonDict: [AnyHashable : Any]? = nil
        let url = URL(string: lookUpStr)
        let session = URLSession.shared
        
        if let usableUrl = url {
            let task = session.dataTask(with: usableUrl, completionHandler: { (data, response, error) in
                if let data = data {
                    do {
                        jsonDict = try JSONSerialization.jsonObject(with: data, options: []) as? [AnyHashable : Any]
                    } catch {
                        print(error)
                    }
                    
                    let jsonResults = jsonDict?["candidates"] as? Array<Any>
                    let addressResult = jsonResults?.first as? Dictionary<String, Any>
                    // Get whether it is open.
                    if let openingHours = addressResult?["opening_hours"] as? Dictionary<String, Any>{
                        if let openStr = openingHours["open_now"] as? Int{
                            if openStr == 1 || openStr == 3{
                                self.filteredNearbySafePlace.append(location as! LocationAnnotation)
                                
                            }
                        }
                    }
                    self.dispatchGroup.leave()
                }
            })
            task.resume()
        }
        
        //        dispatchGroup.notify(queue: .main) {
        //            let currentAn = LocationAnnotation(newTitle: "Current Location", newSubtitle: "", lat: self.currentLocation!.latitude, long: self.currentLocation!.longitude)
        //            self.filteredNearbySafePlace.append(currentAn)
        //        }
    }
    
    @IBOutlet weak var soundPlayButton: UIButton!
    
    @IBAction func playSound(_ sender: Any) {
        if playing {
            player?.stop()
            playing = false
            soundPlayButton.setImage(UIImage(named: "Sound_on"), for: .normal)
        }
        else {
            playSound()
            playing = true
            soundPlayButton.setImage(UIImage(named: "Sound_off"), for: .normal)
        }
        
    }
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "noise", withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            guard let player = player else { return }
            
            // Default to go with full volume
            //**************************************************change to 1.0**************
            MPVolumeView.setVolume(1.0)
            
            player.numberOfLoops = -1
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    
    
    
    @IBAction func showShareLocationBtn(_ sender: Any) {
        if userDataManager.hasHelp {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "shareLocation") as! ShareLocationViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let alert = UIAlertController(title: "Noticfation", message: "You do not have protectee.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    
    @IBOutlet weak var flashButton: UIButton!
    
    var buttonNotPressed: Bool = false
    @IBAction func turnFlash(_ sender: Any) {
        buttonNotPressed = !buttonNotPressed
        
        if flashing {
            toggleFlash()
            flashing = false
            flashButton.setImage(UIImage(named: "Flash_on"), for: .normal)
        } else {
            toggleFlash()
            flashing = true
            flashButton.setImage(UIImage(named: "Flash_off"), for: .normal)
        }
    }
    
    func toggleFlash() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if !self.buttonNotPressed {
                device.torchMode = AVCaptureDevice.TorchMode.off
            }
            else{
                if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                    device.torchMode = AVCaptureDevice.TorchMode.off
                } else {
                    do {
                        try device.setTorchModeOn(level: 1.0)
                    } catch {
                        print(error)
                    }
                }
                
                DispatchQueue.main.asyncAfter(deadline:.now() + 0.2 ) {
                    if self.buttonNotPressed {
                        self.toggleFlash()
                    }
                }
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
    
    // Get opening information from latitude and longitude.
    func getGoogleAdrress(location: MKAnnotation) {
        var error: Error? = nil
        var lookUpStr = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=\(location.title!!)&inputtype=textquery&fields=photos,formatted_address,name,opening_hours,permanently_closed&locationbias=circle:100@\(location.coordinate.latitude),\(location.coordinate.longitude)&key=AIzaSyCniR4gRiVeDqK8NG6gGQnAiQ5JH2N9wvI"
        
        lookUpStr = lookUpStr.replacingOccurrences(of: " ", with: "+")
        var jsonResponse: Data? = nil
        if let url = URL(string: lookUpStr) {
            do {
                jsonResponse = try Data(contentsOf: url)
            } catch {
                print(error)
                // or display a dialog
            }
        }
        var jsonDict: [AnyHashable : Any]? = nil
        do {
            if let jsonResponse = jsonResponse {
                jsonDict = try JSONSerialization.jsonObject(with: jsonResponse, options: []) as? [AnyHashable : Any]
            }
        } catch {
            print(error)
        }
        print(jsonDict as Any)
        let jsonResults = jsonDict?["candidates"] as? Array<Any>
        let addressResult = jsonResults?.first as? Dictionary<String, Any>
        let photos = addressResult?["photos"] as? Array<Any>
        // Get whether it is open.
        if let openingHours = addressResult?["opening_hours"] as? Dictionary<String, Any>{
            if let openStr = openingHours["open_now"] as? Int{
                if openStr == 0 {
                    opening = false
                } else {
                    opening = true
                }
            }
        }
    }
}

extension MPVolumeView {
    static func setVolume(_ volume: Float) {
        let volumeView = MPVolumeView()
        let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider
        slider?.isHidden = true
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            slider?.value = volume
        }
    }
}


