//
//  MainTabNavigationController.swift
//  VicGuardian
//
//  Created by sai ma on 2/5/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation

class MainTabNavigationController: UINavigationController {
    
    var userDataManager: UserDataManager!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDataManager = getAppDelegate().app.userDataManager
        if userDataManager.hasEmergency{
            if TimerManager.seconds != 0 {
                let showTimeVC = self.storyboard!.instantiateViewController(withIdentifier: "showTIme")
                self.viewControllers = [showTimeVC]
            }else{
//                self.performSegue(withIdentifier: "setTimeAlarmSegue", sender: nil)
                let setTimeVC = self.storyboard!.instantiateViewController(withIdentifier: "setTime")
                self.viewControllers = [setTimeVC]
            }
   
        }else{
            let setRealtionshipVC = self.storyboard!.instantiateViewController(withIdentifier: "setRealtionship")
            self.viewControllers = [setRealtionshipVC]
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if userDataManager.hasEmergency{
            if TimerManager.seconds != 0 {
                let showTimeVC = self.storyboard!.instantiateViewController(withIdentifier: "showTIme")
                self.viewControllers = [showTimeVC]
            }else{
                //                self.performSegue(withIdentifier: "setTimeAlarmSegue", sender: nil)
                let setTimeVC = self.storyboard!.instantiateViewController(withIdentifier: "setTime")
                self.viewControllers = [setTimeVC]
            }
            
        }else{
            let setRealtionshipVC = self.storyboard!.instantiateViewController(withIdentifier: "setRealtionship")
            self.viewControllers = [setRealtionshipVC]
        }
    }
    
    
    
    @IBAction func pauseTimeButton(_ sender: Any) {
        
    }
    
}
