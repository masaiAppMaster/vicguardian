//
//  SafePlaceSelectorViewController.swift
//  VicGuardian
//
//  Created by erica12580 on 6/5/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation
import UIKit
import Firebase

protocol SafePlaceSelectorViewControllerDelegate{
    
    func didSaveSelector ()
}


class SafePlaceSelectorViewController: UIViewController, CheckboxDelegate {
    
    
    @IBOutlet weak var policeCheckBox: CCheckbox!
    
    @IBOutlet weak var shopCheckBox: CCheckbox!
    
    @IBOutlet weak var gasCheckBox: CCheckbox!
    
    @IBOutlet weak var railwayCheckBox: CCheckbox!
    
    
    var checkList: Array<Bool> = []
    var delegate: SafePlaceSelectorViewControllerDelegate?
    var policeChecked: Bool = true
    var shopChecked: Bool = true
    var gasChecked: Bool = true
    var railwayChecked: Bool = true
    var userDataManager: UserDataManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDataManager = getAppDelegate().app.userDataManager
        
        policeCheckBox.delegate = self
        shopCheckBox.delegate = self
        gasCheckBox.delegate = self
        railwayCheckBox.delegate = self
        
        policeCheckBox.animation = .showHideTransitionViews
        shopCheckBox.animation = .showHideTransitionViews
        gasCheckBox.animation = .showHideTransitionViews
        railwayCheckBox.animation = .showHideTransitionViews
    }
    
    
    @IBAction func saveButton(_ sender: Any) {
        let ref = Database.database().reference().child("userPreferences").child(userDataManager.uid)
        ref.setValue([
            "policeChecked": policeChecked,
            "shopChecked": shopChecked,
            "gasChecked": gasChecked,
            "railwayChecked": railwayChecked
            ])
        delegate?.didSaveSelector()
        self.navigationController?.popViewController(animated: true)
    }
    
    func didSelect(_ checkbox: CCheckbox) {
        switch checkbox {
        case policeCheckBox:
            policeChecked = true
            break
        case shopCheckBox:
            shopChecked = true
            break
        case gasCheckBox:
            gasChecked = true
            break
        case railwayCheckBox:
            railwayChecked = true
        default:
            break
        }
    }
    
    func didDeselect(_ checkbox: CCheckbox) {
        switch checkbox {
        case policeCheckBox:
            policeChecked = false
            break
        case shopCheckBox:
            shopChecked = false
            break
        case gasCheckBox:
            gasChecked = false
            break
        case railwayCheckBox:
            railwayChecked = false
        default:
            break
        }
    }
    
}
