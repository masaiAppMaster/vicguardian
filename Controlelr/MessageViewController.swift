//
//  IndexViewController.swift
//  VicGuardian
//
//  Created by sai ma on 2/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import UIKit
import Firebase
import AudioToolbox

class MessageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var userDataManager: UserDataManager!
    var relationshipRef: DatabaseReference!
    @IBOutlet weak var tableView: UITableView!
    var app: App!
    var messages: [String] = []
    var helperID: String?
    var protectorNotificationHiden: Bool = true
    var helperNotificationHiden: Bool = true
    
    var subscription: Subscription<UserDataEvent>?
//    var hasNotification: BarNotification!
    
    @IBAction func addEmergencyButton(_ sender: Any) {
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.navigationController?.tabBarItem.badgeValue = " "
        userDataManager = getAppDelegate().app.userDataManager
        relationshipRef = Database.database().reference().child("relationship").child(userDataManager.uid)
//        listenHelper()
//        if self.protectorNotification && self.helperNotification {
//            self.navigationController?.tabBarItem.badgeValue = nil
//        } else {
//            self.navigationController?.tabBarItem.badgeValue = " "
//        }
        subscription = userDataManager.userDataEventDispatcher.subscribeOnMain() { event in
            switch event {
            case .emergencyMessageUpdate: break
//                self.tableView.reloadData()
//                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            case .helperMessageUpdate: break
//                self.tableView.reloadData()
//                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            case .helperIDUpdate: break
//                self.tableView.reloadData()
            }
            if self.userDataManager.hasEmergency {
                if self.userDataManager.emergencyMessageLastReadID == self.userDataManager.emergencyOtherMessages.last?.timeStamp || self.userDataManager.emergencyOtherMessages.last?.timeStamp == nil  {
                    self.protectorNotificationHiden = true
                }else{
                    self.protectorNotificationHiden = false
                    
                }
            } else {
                self.protectorNotificationHiden = true
            }
            
            if self.userDataManager.hasHelp {
                if self.userDataManager.HelperLastReadID == self.userDataManager.helperOtherMessages.last?.timeStamp || self.userDataManager.helperOtherMessages.last?.timeStamp == nil {
                    self.helperNotificationHiden = true
                }else{
                    self.helperNotificationHiden = false
                }
            }else {
                self.helperNotificationHiden = true
            }

            if self.protectorNotificationHiden && self.helperNotificationHiden {
                self.navigationController?.tabBarItem.badgeValue = nil
            } else {
                self.navigationController?.tabBarItem.badgeValue = " "
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        userDataManager = getAppDelegate().app.userDataManager
        relationshipRef = Database.database().reference().child("relationship").child(userDataManager.uid)
        listenHelper()

        
//        self.tableView.reloadData()
    }

    func listenHelper() {
//        relationshipRef.observe(DataEventType.value) {
//            snapShot in
//            self.app = getAppDelegate().app
//            self.userDataManager = getAppDelegate().app.userDataManager
//            self.tableView.reloadData()
////            let helpValue = snapShot.value a.s? NSDictionary
////            self.helperID = helpValue?["helpID"] as? String ?? nil
//
//        }
        
        subscription = userDataManager.userDataEventDispatcher.subscribeOnMain() { event in
            switch event {
            case .emergencyMessageUpdate:
                self.tableView.reloadData()
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            case .helperMessageUpdate:
                self.tableView.reloadData()
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            case .helperIDUpdate:
                self.tableView.reloadData()
            }

            if self.protectorNotificationHiden && self.helperNotificationHiden {
                self.navigationController?.tabBarItem.badgeValue = nil
            } else {
                self.navigationController?.tabBarItem.badgeValue = " "
            }

        }
    }
    
    deinit {
        subscription?.unsubscribe()
    }

    
// 所有的没设置成
//    func setupData() {
//        messageRef.observe(DataEventType.value) { snapShot in
//            let enumerator = snapShot.children
//            while let snapShotValue = enumerator.nextObject() as? DataSnapshot {
//                let dict = snapShotValue.value as! [String: String]
//                self.messages.append(dict["from"]!)
//                self.messages.append(dict["from"]!)
//                let unique = Array(Set(self.messages))
//                print(unique)
//            }
//        }
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
//        app = getAppDelegate().app
        userDataManager = getAppDelegate().app.userDataManager
        self.tableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
//        return userDataManager.hasEmergency ? 1 : 0

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        if index == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "messageGroupCell", for: indexPath) as! MessageGroupCell
            if userDataManager.hasEmergency {
                
                cell.nameLabel?.text = userDataManager.emergencyName
                cell.protectorMessageLabel.text = userDataManager.emergencyOtherMessages.last?.text
                if userDataManager.emergencyMessageLastReadID == userDataManager.emergencyOtherMessages.last?.timeStamp || userDataManager.emergencyOtherMessages.last?.timeStamp == nil  {
                    cell.noticReceiveMessage.isHidden = true
                    protectorNotificationHiden = true
                }else{
                    cell.noticReceiveMessage.isHidden = false
                    protectorNotificationHiden = false

                }
            } else {
                cell.noticReceiveMessage.isHidden = true
                protectorNotificationHiden = true
                cell.nameLabel?.text = "No protector, Clik here to add"
                cell.protectorMessageLabel.text = ""
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "helperGroupCell", for: indexPath) as! HelperGroupCell
            if userDataManager.hasHelp {
                cell.isHidden = false
                cell.helperNameLabel?.text = userDataManager.helperName
                cell.helperMessageLabel?.text = userDataManager.helperOtherMessages.last?.text
                if userDataManager.HelperLastReadID == userDataManager.helperOtherMessages.last?.timeStamp || userDataManager.helperOtherMessages.last?.timeStamp == nil {
                    cell.noticReceiveMessage.isHidden = true
                    helperNotificationHiden = true
                }else{
                    cell.noticReceiveMessage.isHidden = false
                    helperNotificationHiden = false
                }
            }else {
                cell.isHidden = true
                cell.noticReceiveMessage.isHidden = true
                cell.helperNameLabel?.text = "Do not have helper"
                cell.helperMessageLabel.text = ""
                helperNotificationHiden = true
            }
            if self.protectorNotificationHiden && self.helperNotificationHiden {
                self.navigationController?.tabBarItem.badgeValue = nil
            } else {
                self.navigationController?.tabBarItem.badgeValue = " "
            }
            return cell
            
        }
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "messageGroupCell", for: indexPath) as! MessageGroupCell
//        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        if index == 0 {
            if userDataManager.hasEmergency {
                userDataManager.setEmergencyLastReadID()
                performSegue(withIdentifier: "goToChat", sender: userDataManager.emergencyID)
            } else {
                performSegue(withIdentifier: "goToAdd", sender: nil)
            }
        } else {
            if userDataManager.hasHelp {
                userDataManager.setHelperLastReadID()
                performSegue(withIdentifier: "goToHelperChat", sender: userDataManager.helperID)
            } else {
                noHelperAlert()
            }
        }
    }
    
    func noHelperAlert() {
        let alert = UIAlertController(title: "My Alert", message: "Do not have helper", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
}


class MessageGroupCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var protectorMessageLabel: UILabel!
    @IBOutlet weak var noticReceiveMessage: UIImageView!
    
}



class HelperGroupCell: UITableViewCell {
    @IBOutlet weak var helperNameLabel: UILabel!
    
    @IBOutlet weak var helperMessageLabel: UILabel!
    @IBOutlet weak var noticReceiveMessage: UIImageView!
}

