//
//  ViewController.swift
//  POC-message
//
//  Created by sai ma on 27/2/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import UIKit
import Firebase

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var userDataManager: UserDataManager!
    var userOneID = ""
    var userTwoID = ""
    public var meRef: DatabaseReference!
    public var otherRef: DatabaseReference!
    @IBOutlet weak var textfield: UITextField!
    var messages: [SDChatModel] = []
    var otherMessages: [SDChatModel] = []
    var meMessages: [SDChatModel] = []
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    
    let semaphore = DispatchSemaphore.init(value: 1)
    
    @IBAction func load(_ sender: Any) {
        DispatchQueue.global().async {
            self.semaphore.wait()
            let ref = Database.database().reference().child("message").child(self.userOneID).child(self.userTwoID)
            var query = ref.queryOrderedByKey()
            print(query)
            if self.messages.isEmpty == false {
                query = query.queryEnding(atValue: self.messages[0].key)
            }
            query.queryLimited(toLast: 3).observeSingleEvent(of: DataEventType.value) { snapShot in
                
                var newMessage: [SDChatModel] = []
                let enumerator = snapShot.children
                
                while let snapShotValue = enumerator.nextObject() as? DataSnapshot {
                    
                    let dict = snapShotValue.value as! [String: String]
                    let message = SDChatModel()
                    message.text = dict["text"]!
                    message.timeStamp = Double(dict["timestamp"]!)!
                    message.key = snapShotValue.key
                    newMessage.append(message)
                }
                if self.messages.isEmpty == false {
                    newMessage = Array(newMessage.dropLast())
                }
                newMessage.append(contentsOf: self.messages)
                self.messages = newMessage
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.semaphore.signal()
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDataManager = getAppDelegate().app.userDataManager
        userOneID = userDataManager.uid
        userTwoID = userDataManager.emergencyID!
        listenMeChatData()
        addTapToHideKeyboardGesture()
        //        listenOtherChatData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerKeyboardNotification()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        unregisterKeyboardNotification()
    }
    
    func unregisterKeyboardNotification() {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    func registerKeyboardNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        guard let kbFrame = getKeyboardSize(notification: notification as Notification),
            let duration = getDuration(notification: notification as Notification),
            let curve = getCurve(notification: notification as Notification)
            else { return }
        
        
        guard let window = self.view.window else { return }
        
        let viewSafeAreaFrame = self.view.frame.inset(by: self.view.safeAreaInsets)
        
        let viewFrameInWindow = window.convert(viewSafeAreaFrame, from: nil)
        
        let coveredFrame = viewFrameInWindow.intersection(kbFrame)
        
        guard !coveredFrame.isNull else { return }
        
        //set the content inset
        self.bottomConstraint.constant = coveredFrame.height
        
        UIView.animate(withDuration: duration, delay: 0.0, options: UIView.AnimationOptions(rawValue: UInt(curve)), animations: {
            self.view.layoutIfNeeded()
            
        }, completion: { _ in
        })
//        self.scrollToBottom()
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        bottomConstraint.constant = 0
        
    }
    
    func listenMeChatData() {
        meRef = Database.database().reference().child("message").child(userOneID).child(userTwoID)
        
        meRef.observe(DataEventType.value) { snapShot in
            let enumerator = snapShot.children
            self.messages = []
            self.meMessages = []
            while let snapShotValue = enumerator.nextObject() as? DataSnapshot {
                let dict = snapShotValue.value as! [String: String]
                let message = SDChatModel()
                message.imageName = dict["image"] ?? ""
                message.text = dict["text"] ?? ""
                message.timeStamp = Double(dict["timestamp"]!)!
                self.meMessages.append(message)
            }
            for otherSingleMessage in self.otherMessages {
                self.messages.append(otherSingleMessage)
            }
            for meSingleMessage in self.meMessages {
                self.messages.append(meSingleMessage)
            }
            self.sortMessage()
            self.listenOtherChatData()
            self.tableView.reloadData()
            self.scrollToBottom()
        }
    }
    
    func listenOtherChatData() {
        otherRef = Database.database().reference().child("message").child(userTwoID).child(userOneID)
        otherRef.observe(DataEventType.value) { snapShot in
            let enumerator = snapShot.children
            self.otherMessages = []
            self.messages = []
            while let snapShotValue = enumerator.nextObject() as? DataSnapshot {
                let dict = snapShotValue.value as! [String: String]
                let message = SDChatModel()
                message.messageType = .others
                message.imageName = dict["image"] ?? ""
                message.text = dict["text"] ?? ""
                message.timeStamp = Double(dict["timestamp"]!)!
                self.otherMessages.append(message)
                self.sortOtherMessage()
                
            }
            for otherSingleMessage in self.otherMessages {
                self.messages.append(otherSingleMessage)
            }
            for meSingleMessage in self.meMessages {
                self.messages.append(meSingleMessage)
            }
            self.sortMessage()
            self.tableView.reloadData()
            self.scrollToBottom()
        }
    }
    
    func sortMessage() {
        let ordered = self.messages.sorted(by: {$0.timeStamp < $1.timeStamp})
//        for oneOrdered in ordered {
//            print(oneOrdered)
//        }
        self.messages = ordered
    }
    
    func sortOtherMessage() {
        let ordered = self.otherMessages.sorted(by: {$0.timeStamp < $1.timeStamp})
//        for oneOrdered in ordered {
//            print(oneOrdered)
//        }
        self.otherMessages = ordered
    }
    
    @IBAction func clickButton(_ sender: Any) {
        
        let text = textfield.text ?? ""
        self.scrollToBottom()
        send(text: text)
        textfield.text = ""
    }
    
    @IBOutlet weak var helpMessageButton: UIButton!
    
    @IBAction func helpMessageButton(_ sender: Any) {
        let text = "i need help,SOS"
//        let indexPath = IndexPath(row: self.messages.count - 1, section: 0)
//        self.tableView.insertRows(at: [indexPath], with: .bottom)
//        self.scrollToBottom()
        send(text: text)
    }
    
    func send(text: String) {
        let date = Date()
        let timeInterval = date.timeIntervalSince1970
        let timestamp = timeInterval.description.replacingOccurrences(of: ".", with: "").padding(toLength: 16, withPad: "0", startingAt: 0)
        let childRef = meRef.childByAutoId()
        childRef.setValue([
            "text" : text,
            "type" : "text",
            "timestamp": timestamp
            ])
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let model = messages[index]
        if model.isImage {
            var cell: SDChatTableViewImageCell
            if  model.messageType == .me {
                cell = tableView.dequeueReusableCell(withIdentifier: "meImageCell", for: indexPath) as! SDChatTableViewImageCell
                let timeShowData = model.timeStamp/1000000
                cell.chatImageView.image = UIImage(named: "1446550298.jpg")
//                cell.timeLabel.text = getDateFromTimeStamp(timeStamp: timeShowData)
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "otherImageCell", for: indexPath) as! SDChatTableViewImageCell
                
            }
            cell.model = model
            return cell
        } else {
            var cell: SDChatTableViewTextCell
            if  model.messageType == .me {
                cell = tableView.dequeueReusableCell(withIdentifier: "meTextCell", for: indexPath) as! SDChatTableViewTextCell
                let timeShowData = model.timeStamp/1000000
//                cell.timeLabel.text = getDateFromTimeStamp(timeStamp: timeShowData)
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "otherTextCell", for: indexPath) as! SDChatTableViewTextCell
            }
            cell.model = model
            return cell
        }
    }
    
    func getDateFromTimeStamp(timeStamp : Double) -> String {
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
//        dayTimePeriodFormatter.dateFormat = "dd MMM YY, hh:mm a"
        dayTimePeriodFormatter.dateFormat = "dd MMM, HH:mm"
        // UnComment below to get only time
        //  dayTimePeriodFormatter.dateFormat = "hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
        
    }
    private func scrollToBottom() {
        let messageCount = messages.count
        if messageCount > 0 {
            let indexPath = IndexPath(row: messageCount - 1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    private func getKeyboardSize(notification: Notification) -> CGRect?
    {
        return (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
    }
    
    private func getDuration(notification: Notification) -> Double?
    {
        return (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue
    }
    
    private func getCurve(notification: Notification) -> Int?
    {
        return (notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.intValue
    }
    
    
    func addTapToHideKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(tap:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(tap: UITapGestureRecognizer) {
        self.textfield.endEditing(true)
    }
}






class SDChatModel {
    public var text = ""
    var iconName = ""
    var imageName = ""
    var timeStamp: Double = 0.0
    var key: String = ""
    var messageType: SDMessageType = .me
    var isImage: Bool {
        return imageName.isEmpty == false
    }
}



enum SDMessageType {
    case me
    case others
}


class SDChatTableViewTextCell: UITableViewCell {
    @IBOutlet weak var message: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    //    @IBOutlet weak var timeLabel: UILabel!
    var model: SDChatModel?{
        didSet {
            guard let model = self.model else { return }
            self.message.text = model.text
        }
    }
}


class SDChatTableViewImageCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var chatImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var shareLocationBtn: UIButton!
    
    
    var model: SDChatModel?{
        didSet{
            //必须进行的步奏，强制转化？
//            guard let model = self.model else { return }
//            self.iconImageView.image = UIImage(named: model.iconName)
//            //把model传递给MMainstorybord里面的头像
//            self.chatImageView.image = UIImage(named: model.imageName)
        }
    }
    @IBAction func shareLocationInChat(_ sender: Any) {
    }
    
}
















class HelperChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var userDataManager: UserDataManager!
    var userOneID = ""
    var userTwoID = ""
    public var meRef: DatabaseReference!
    public var otherRef: DatabaseReference!
    @IBOutlet weak var textfield: UITextField!
    var messages: [SDChatModel] = []
    var otherMessages: [SDChatModel] = []
    var meMessages: [SDChatModel] = []
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    let semaphore = DispatchSemaphore.init(value: 1)
    
    @IBAction func load(_ sender: Any) {
        DispatchQueue.global().async {
            self.semaphore.wait()
            let ref = Database.database().reference().child("message").child(self.userOneID).child(self.userTwoID)
            var query = ref.queryOrderedByKey()
            print(query)
            if self.messages.isEmpty == false {
                query = query.queryEnding(atValue: self.messages[0].key)
            }
            query.queryLimited(toLast: 3).observeSingleEvent(of: DataEventType.value) { snapShot in
                
                var newMessage: [SDChatModel] = []
                let enumerator = snapShot.children
                
                while let snapShotValue = enumerator.nextObject() as? DataSnapshot {
                    
                    let dict = snapShotValue.value as! [String: String]
                    let message = SDChatModel()
                    message.text = dict["text"]!
                    message.timeStamp = Double(dict["timestamp"]!)!
                    message.key = snapShotValue.key
                    newMessage.append(message)
                }
                if self.messages.isEmpty == false {
                    newMessage = Array(newMessage.dropLast())
                }
                newMessage.append(contentsOf: self.messages)
                self.messages = newMessage
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.semaphore.signal()
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDataManager = getAppDelegate().app.userDataManager
        userOneID = userDataManager.uid
        userTwoID = userDataManager.helperID!
        listenMeChatData()
        addTapToHideKeyboardGesture()
      
        //        listenOtherChatData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerKeyboardNotification()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        unregisterKeyboardNotification()
    }
    
    func unregisterKeyboardNotification() {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    func registerKeyboardNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        guard let kbFrame = getKeyboardSize(notification: notification as Notification),
            let duration = getDuration(notification: notification as Notification),
            let curve = getCurve(notification: notification as Notification)
            else { return }
        
        
        guard let window = self.view.window else { return }
        
        let viewSafeAreaFrame = self.view.frame.inset(by: self.view.safeAreaInsets)
        
        let viewFrameInWindow = window.convert(viewSafeAreaFrame, from: nil)
        
        let coveredFrame = viewFrameInWindow.intersection(kbFrame)
        
        guard !coveredFrame.isNull else { return }
        
        //set the content inset
        self.bottomConstraint.constant = coveredFrame.height
        
        UIView.animate(withDuration: duration, delay: 0.0, options: UIView.AnimationOptions(rawValue: UInt(curve)), animations: {
            self.view.layoutIfNeeded()
            
        }, completion: { _ in
        })
        self.scrollToBottom()
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        bottomConstraint.constant = 0
        
    }
    
    func listenMeChatData() {
        meRef = Database.database().reference().child("message").child(userOneID).child(userTwoID)
        
        meRef.observe(DataEventType.value) { snapShot in
            let enumerator = snapShot.children
            self.messages = []
            self.meMessages = []
            while let snapShotValue = enumerator.nextObject() as? DataSnapshot {
                let dict = snapShotValue.value as! [String: String]
                let message = SDChatModel()
                message.text = dict["text"] ?? ""
                message.timeStamp = Double(dict["timestamp"]!)!
                self.meMessages.append(message)
            }
            for otherSingleMessage in self.otherMessages {
                self.messages.append(otherSingleMessage)
            }
            for meSingleMessage in self.meMessages {
                self.messages.append(meSingleMessage)
            }
            self.sortMessage()
            self.listenOtherChatData()
            self.tableView.reloadData()
            self.scrollToBottom()
        }
    }
    
    func listenOtherChatData() {
        otherRef = Database.database().reference().child("message").child(userTwoID).child(userOneID)
        otherRef.observe(DataEventType.value) { snapShot in
            let enumerator = snapShot.children
            self.otherMessages = []
            self.messages = []
            while let snapShotValue = enumerator.nextObject() as? DataSnapshot {
//                let dict = snapShotValue.value as! [String: String]
//                let message = SDChatModel()
//                message.messageType = .others
//                message.text = dict["text"] ?? ""
//                message.timeStamp = Double(dict["timestamp"]!)!
//                self.otherMessages.append(message)
//                self.sortOtherMessage()
                let dict = snapShotValue.value as! [String: String]
                let message = SDChatModel()
                message.messageType = .others
                message.imageName = dict["image"] ?? ""
                message.text = dict["text"] ?? ""
                message.timeStamp = Double(dict["timestamp"]!)!
                self.otherMessages.append(message)
                self.sortOtherMessage()
                
            }
            for otherSingleMessage in self.otherMessages {
                self.messages.append(otherSingleMessage)
            }
            for meSingleMessage in self.meMessages {
                self.messages.append(meSingleMessage)
            }
            self.sortMessage()
            self.tableView.reloadData()
            self.scrollToBottom()
        }
    }
    
    func sortMessage() {
        let ordered = self.messages.sorted(by: {$0.timeStamp < $1.timeStamp})
        for oneOrdered in ordered {
            print(oneOrdered)
        }
        self.messages = ordered
    }
    
    func sortOtherMessage() {
        let ordered = self.otherMessages.sorted(by: {$0.timeStamp < $1.timeStamp})
        for oneOrdered in ordered {
            print(oneOrdered)
        }
        self.otherMessages = ordered
    }
    
    @IBAction func clickButton(_ sender: Any) {
        
        let text = textfield.text ?? ""
        self.scrollToBottom()
        send(text: text)
        textfield.text = ""
    }
    
    
    @IBAction func helpMessageButton(_ sender: Any) {
        let text = "i need help,SOS"
//        let indexPath = IndexPath(row: self.messages.count - 1, section: 0)
//        self.tableView.insertRows(at: [indexPath], with: .bottom)
//        self.scrollToBottom()
        send(text: text)
    }
    
    func send(text: String) {
        let date = Date()
        let timeInterval = date.timeIntervalSince1970
        let timestamp = timeInterval.description.replacingOccurrences(of: ".", with: "").padding(toLength: 16, withPad: "0", startingAt: 0)
        let childRef = meRef.childByAutoId()
        childRef.setValue([
            "text" : text,
            "type" : "text",
            "timestamp": timestamp
            ])
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let model = messages[index]
        if model.isImage {
            var cell: SDChatTableViewImageCell
            if  model.messageType == .me {
                cell = tableView.dequeueReusableCell(withIdentifier: "meImageCell", for: indexPath) as! SDChatTableViewImageCell
                let timeShowData = model.timeStamp/1000000
                cell.timeLabel.text = getDateFromTimeStamp(timeStamp: timeShowData)
//                cell.timeLabel.text = String(model.timeStamp)
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "otherImageCell", for: indexPath) as! SDChatTableViewImageCell
                cell.shareLocationBtn.setImage(UIImage(named: "1446550298.jpg"), for: .normal)
                let timeShowData = model.timeStamp/1000000
                cell.timeLabel.text = getDateFromTimeStamp(timeStamp: timeShowData)
                
            }
            cell.model = model
            return cell
        } else {
            var cell: SDChatTableViewTextCell
            if  model.messageType == .me {
                cell = tableView.dequeueReusableCell(withIdentifier: "meTextCell", for: indexPath) as! SDChatTableViewTextCell
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "otherTextCell", for: indexPath) as! SDChatTableViewTextCell
                let timeShowData = model.timeStamp/1000000
                cell.timeLabel.text = getDateFromTimeStamp(timeStamp: timeShowData)
            }
            cell.model = model
            return cell
        }
    }
    
    
    func getDateFromTimeStamp(timeStamp : Double) -> String {
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        //        dayTimePeriodFormatter.dateFormat = "dd MMM YY, hh:mm a"
        dayTimePeriodFormatter.dateFormat = "dd MMM, HH:mm"
        // UnComment below to get only time
        //  dayTimePeriodFormatter.dateFormat = "hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
        
    }
    
    
    private func scrollToBottom() {
        let messageCount = messages.count
        if messageCount > 0 {
            let indexPath = IndexPath(row: messageCount - 1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    private func getKeyboardSize(notification: Notification) -> CGRect?
    {
        return (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
    }
    
    private func getDuration(notification: Notification) -> Double?
    {
        return (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue
    }
    
    private func getCurve(notification: Notification) -> Int?
    {
        return (notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.intValue
    }
    
    
    func addTapToHideKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(tap:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(tap: UITapGestureRecognizer) {
        self.textfield.endEditing(true)
    }
}
