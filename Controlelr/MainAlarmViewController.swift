//
//  MainAlarmViewController.swift
//  VicGuardian
//
//  Created by sai ma on 16/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import UIKit
import Firebase
import AudioToolbox



class MainAlarmViewController: UIViewController {
    
    
    var userDataManager: UserDataManager!
    public var meRef: DatabaseReference!
    var time: Double = 0.0
    var messageInfo = ""
    
    
    @IBOutlet weak var timeLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDataManager = getAppDelegate().app.userDataManager
        showPauseButton.backgroundColor = UIColor.orange
        TimerManager.timeString = { [weak self] value, finish in
            guard let self = self else { return }
            if TimerManager.seconds == 120 {
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            }
            self.timeLabel.text = value
            if finish {
                self.send(text: self.messageInfo)
                TimerManager.destroy()
                self.timeLabel.text = "Sent"
                self.displayFinished()
                
            }
        }
    }
  
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "goToEditTime" {
//            if let destVC = segue.destination as? AlarmAddEditViewController {
//                destVC.delegate = self
//            }
//        }
//    }

    func displayFinished() {
        let alert = UIAlertController(title: "Information", message: "The message has been sent to your protector.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "setTime") as! AlarmAddEditViewController
            self.navigationController?.setViewControllers([vc], animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func send(text: String) {
        meRef = Database.database().reference().child("message").child(userDataManager.uid).child(userDataManager.emergencyID!)
        let date = Date()
        let timeInterval = date.timeIntervalSince1970
        let timestamp = timeInterval.description.replacingOccurrences(of: ".", with: "").padding(toLength: 16, withPad: "0", startingAt: 0)
        let childRef = meRef.childByAutoId()
        childRef.setValue([
            "text" : text,
            "type" : "text",
            "timestamp": timestamp
            ])
    }
    
    
    @IBAction func editButton(_ sender: Any) {
        if userDataManager.hasEmergency {
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "setTime") as! AlarmAddEditViewController
            vc.messageLabel.text = "nhik"
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
//            performSegue(withIdentifier: "setTimeSegue", sender: nil)
        } else {
            performSegue(withIdentifier: "setRealationshipSegue", sender: nil)
        }
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        self.timeLabel.text = "00:00:00"
        TimerManager.seconds = 0
        TimerManager.destroy()
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "setTime") as! AlarmAddEditViewController
        self.navigationController?.setViewControllers([vc], animated: true)
    }
    
//    func alarmAddEditViewControllerDidSet(time: Double, message: String) {
//        TimerManager.destroy()
//        self.time = time
//        self.messageInfo = message
//        TimerManager.start(withSeconds: Int(self.time))
//    }
    @IBOutlet weak var showPauseButton: UIButton!{
        didSet{
           
            if TimerManager.pauseBool {
                showPauseButton.setTitle("Continue", for: .normal)
                
            }else{
                showPauseButton.setTitle("Pause", for: .normal)
                
            }
        }
    }
    
    @IBAction func pauseButton(_ sender: Any) {
         self.view.showLoadingIndicator()
        if TimerManager.pauseBool{
            TimerManager.continuePasue()
            showPauseButton.setTitle("Pause", for: .normal)
            showPauseButton.backgroundColor = UIColor.orange
        }else {
            TimerManager.pause()
            showPauseButton.setTitle("Continue", for: .normal)
            showPauseButton.backgroundColor = #colorLiteral(red: 0, green: 0.5651715994, blue: 0.1482007504, alpha: 1)
        }
        self.view.hideLoadingIndicator()
    }
    
    
    
}


