//
//  ShareLocationViewController.swift
//  VicGuardian
//
//  Created by sai ma on 4/5/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import Firebase

class ShareLocationViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    var locationManager: LocationManager!
    var sharedLocation: LocationAnnotation!
    var helperRef: DatabaseReference!
    var userDataManger: UserDataManager!
    var firstUpdate = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDataManger = getAppDelegate().app.userDataManager
        locationManager = getAppDelegate().app.locationManager
//        helperRef = Database.database().reference().child("location").child(userDataManger.helperID ?? "")
        helperRef = Database.database().reference().child("location").child(userDataManger.helperID!)
//        let currentLocation = CLLocationCoordinate2DMake(locationManager.currentLocation.latitude, locationManager.currentLocation.longitude)
//        let currentLocation = locationManager.currentLocation

        
        helperRef.observe(DataEventType.value) { snapShot in
            self.mapView.removeAnnotations(self.mapView.annotations)
            let dic = snapShot.value as? NSDictionary
            let lat = dic?["lat"] as! Double
            let long = dic?["long"] as! Double
            let startLocation = CLLocationCoordinate2DMake(lat, long)
            self.sharedLocation = LocationAnnotation(newTitle: "Your Protectee Location", newSubtitle: "", lat: startLocation.latitude, long: startLocation.longitude)
            self.mapView.addAnnotation(self.sharedLocation)
            self.mapView.showsUserLocation = true
            self.updateAnnotation(latitude: self.sharedLocation.coordinate.latitude, longitude: self.sharedLocation.coordinate.longitude)
            if self.firstUpdate {
            let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
            let region = MKCoordinateRegion(center: startLocation, span: span)
            self.mapView.setRegion(region, animated: true)
                self.firstUpdate = false
            }
            
        }
//        let startLocation = CLLocationCoordinate2DMake(-37.877512, 145.044216)
//        sharedLocation = LocationAnnotation(newTitle: "shared Location", newSubtitle: "", lat: startLocation.latitude, long: startLocation.longitude)
//        mapView.addAnnotation(sharedLocation)
        
            mapView.showsUserLocation = true
        
//        Timer.scheduledTimer(timeInterval: 2, target:self, selector: #selector(ShareLocationViewController.update), userInfo: nil, repeats: true)
    }
    
    
    @IBAction func callFunction(_ sender: Any) {
        
        guard let number = URL(string: "tel://" + "\(userDataManger.phone)") else { return }
        UIApplication.shared.open(number)
    }
    @IBAction func callPoliceBtn(_ sender: Any) {
        guard let number = URL(string: "tel://" + "\(000000)") else { return }
        UIApplication.shared.open(number)
    }
    
    
    func updateAnnotation(latitude: Double, longitude: Double)
    {
        sharedLocation.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
    }
    
    @objc func update() -> Void {
        updateAnnotation(latitude: sharedLocation.coordinate.latitude + 0.01, longitude: sharedLocation.coordinate.longitude)
    }
    
    
}
