//
//  RecommendListViewController.swift
//  VicGuardian
//
//  Created by erica12580 on 2/5/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import CoreLocation
import MapKit

//enum  RecommendListDataEvent{
//    case listneChange
//    case listneChange2
//}

protocol RecommendListViewControllerDelegate{
    
    func didSelectSafePlace (_ annotation: LocationAnnotation)
}

class RecommendListViewController: UITableViewController{
    //    public let listDataEventDispatcher = EventDispatcher<RecommendListDataEvent>()
    //    var subscription: Subscription<RecommendListDataEvent>?
    @objc var locationList: NSMutableArray
    @objc var distanceList: NSMutableArray
    var delegate: RecommendListViewControllerDelegate?
    var passedAnnotation: [LocationAnnotation]?
    var meRef: DatabaseReference!
    var userDataManager: UserDataManager!
    var myArray = [Any]()
    let semaphore = DispatchSemaphore.init(value: 1)
    let dispatchGroup = DispatchGroup()
    var formattedAddress: String?
    
    required init?(coder aDecoder: NSCoder) {
        locationList = NSMutableArray()
        distanceList = NSMutableArray()
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        listen()
        let currentAn = passedAnnotation?.last
        passedAnnotation?.removeLast()
        
        userDataManager = getAppDelegate().app.userDataManager
        for locAn in self.passedAnnotation!
        {
            let request = MKDirections.Request()
            let sourceP = CLLocationCoordinate2DMake((currentAn?.coordinate.latitude)!, (currentAn?.coordinate.longitude)!)
            let desP = CLLocationCoordinate2DMake(locAn.coordinate.latitude, locAn.coordinate.longitude)
            let destination = MKPlacemark(coordinate: desP)
            let source = MKPlacemark(coordinate: sourceP)
            request.source = MKMapItem(placemark: source)
            request.destination = MKMapItem(placemark: destination)
            request.transportType = MKDirectionsTransportType.walking
            request.requestsAlternateRoutes = true
            let directions = MKDirections(request: request)
            dispatchGroup.enter()
            
            directions.calculate { (response, error) in
                if var route = response?.routes {
                    route.sort(by: {$0.expectedTravelTime < $1.expectedTravelTime})
                    let quickestRoute: MKRoute = route[0]
                    var dict = Dictionary<String, AnyObject>()
                    dict["distance"] = quickestRoute.distance as AnyObject
                    dict["annotation"] = locAn
                    self.myArray.append(dict as AnyObject)
                    
                    self.dispatchGroup.leave()
                } else {
                    self.dispatchGroup.leave()
                }
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            self.myArray.sort{
                (($0 as! Dictionary<String, AnyObject>)["distance"] as! Int) < (($1 as! Dictionary<String, AnyObject>)["distance"] as! Int)
                
            }
            for item in self.myArray{
                let dict = item as! Dictionary<String, AnyObject>
                self.locationList.add(dict["annotation"] as! LocationAnnotation)
                self.distanceList.add(dict["distance"] as! Int)
            }
            self.tableView.reloadData()
        }
    }
    
    //    deinit {
    //        subscription?.unsubscribe()
    //    }
    
    //    func listen() {
    //        subscription = listDataEventDispatcher.subscribeOnMain() { event in
    //            switch event {
    //            case .listneChange:
    //                self.myArray.sort{
    //                    (($0 as! Dictionary<String, AnyObject>)["d"] as! Int) < (($1 as! Dictionary<String, AnyObject>)["d"] as! Int)
    //                }
    //                for item in self.myArray{
    //                    let dict = item as! Dictionary<String, AnyObject>
    //                    self.locationList.add(dict["distance"] as! LocationAnnotation)
    //                }
    //                self.tableView.reloadData()
    //            case .listneChange2: break
    //            }
    //        }
    //    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Prompt user to navigation to selected safe place.
        let annotation = locationList.object(at: indexPath.row) as!
        LocationAnnotation
        
        // Get address and send the message
        getAddressInfo(location: annotation)
        
        let selectedAnnotation = MKPlacemark.init(coordinate: annotation.coordinate)
        let mapItem = MKMapItem(placemark: selectedAnnotation)
        mapItem.name = annotation.title!
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeWalking]
        mapItem.openInMaps(launchOptions: launchOptions)
        self.navigationController?.popViewController(animated: true)
    }
    
    // Get address from latitude and longitude.
    func getAddressInfo(location: MKAnnotation) {
        var lookUpStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(location.coordinate.latitude),\(location.coordinate.longitude)&key=AIzaSyCniR4gRiVeDqK8NG6gGQnAiQ5JH2N9wvI"
        
        lookUpStr = lookUpStr.replacingOccurrences(of: " ", with: "+")
        
        var jsonDict: [AnyHashable : Any]? = nil
        let url = URL(string: lookUpStr)
        let session = URLSession.shared
        if let usableUrl = url {
            let task = session.dataTask(with: usableUrl, completionHandler: { (data, response, error) in
                if let data = data {
                    do {
                        jsonDict = try JSONSerialization.jsonObject(with: data, options: []) as? [AnyHashable : Any]
                    } catch {
                        print(error)
                    }
                    
                    let jsonResults = jsonDict?["results"] as? Array<Any>
                    let addressResult = jsonResults?.first as? Dictionary<String, Any>
                    self.formattedAddress = addressResult?["formatted_address"] as! String
                    
                    // Send message to protector.
                    var text = ""
                    text = "I am heading to this safe place! \nName: \(location.title!!) \nAddress: \(self.formattedAddress!) \nLatitude: \(location.coordinate.latitude) \nLongitude: \(location.coordinate.longitude)"
                    self.send(text: text)
                }
            })
            task.resume()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.locationList.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let locAn: LocationAnnotation = self.locationList.object(at: indexPath.row) as! LocationAnnotation
        let distance: Int = self.distanceList.object(at: indexPath.row) as! Int
        
        cell.textLabel!.text = locAn.title
        cell.detailTextLabel!.text = locAn.subtitle
        
        let label = UILabel.init(frame: CGRect(x:0,y:0,width:78,height:20))
        label.text = " " + String(Double(distance)/1000.0) + " km"
        cell.accessoryView = label
        
        return cell
    }
    
    func send(text: String) {
        if userDataManager.hasEmergency {
            meRef = Database.database().reference().child("message").child(userDataManager.uid).child(userDataManager.emergencyID!)
            let date = Date()
            let timeInterval = date.timeIntervalSince1970
            let timestamp = timeInterval.description.replacingOccurrences(of: ".", with: "").padding(toLength: 16, withPad: "0", startingAt: 0)
            let childRef = meRef.childByAutoId()
            childRef.setValue([
                "text" : text,
                "type" : "text",
                "timestamp": timestamp
                ])
        }
    }
}
