//
//  SurburbViewController.swift
//  VicGuardian
//
//  Created by sai ma on 7/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import UIKit
import Firebase

protocol SurburbTableViewControllerProtocl: class {
    func surburbTableViewControllerProtocl(didSelect suburb: String)
}

class SurburbTableViewController: UITableViewController{
    var surburbRef: DatabaseReference!
    var surburb = [String]()
    var data: [SurburbTableViewCell] = []
    
    var selectedSuburb: String? = nil
    
    weak var delegate: SurburbTableViewControllerProtocl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getSurburb()
    }
    

    func getSurburb() {
        surburbRef = Database.database().reference().child("VicData")
        surburbRef.observe(DataEventType.value) { snapShot in
                let emumerator = snapShot.children
                while let snapShotValue = emumerator.nextObject() as? DataSnapshot {
                    let dict = snapShotValue.value as! [String: String]
                    self.surburb.append(dict["Suburb"]!)
                }
            self.tableView.reloadData()
        }
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return surburb.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SurburbTableViewCell
        let index = indexPath.row
        cell.surburbName.text = surburb[index]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.surburbTableViewControllerProtocl(didSelect: surburb[indexPath.row])
        self.navigationController?.popViewController(animated: true)
    }
    
}



class SurburbTableViewCell: UITableViewCell {
    @IBOutlet weak var surburbName: UILabel!
}
