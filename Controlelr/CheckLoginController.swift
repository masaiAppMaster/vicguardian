//
//  CheckLoginController.swift
//  VicGuardian
//
//  Created by sai ma on 16/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import UIKit
import Firebase

class CheckLoginController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.value(forKey: "hasSignin") == nil {
            try? Auth.auth().signOut()
        }
        Auth.auth().addStateDidChangeListener { auth, user in
            if let user = user {
                getAppDelegate().app.login(uid: user.uid)
                self.performSegue(withIdentifier: "goToHomePage", sender: nil)
            } else {
                self.performSegue(withIdentifier: "goToNav", sender: nil)
            }
        }
    }
}
