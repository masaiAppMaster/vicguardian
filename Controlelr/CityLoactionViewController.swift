//
//  CityLoactionViewController.swift
//  VicGuardian
//
//  Created by sai ma on 5/5/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import UIKit
import MapKit



//-37.813283, 144.973973
//-37.817602, 144.959096

class CityLoactionViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    var dict: [String : Double] = [:]
    var cityAnimation = CityAnimation()
    var resultSearchController: UISearchController!

    var routePolyLine: MKPolyline?
    var annotation: MKAnnotation?
    @IBOutlet weak var searchBarPlaceHolder: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable
//        locationSearchTable.automaticallyAdjustsScrollViewInsets = false
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        locationSearchTable.delegate = self
        resultSearchController.searchResultsUpdater = locationSearchTable
        let searchBar = resultSearchController.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Input your destination"
//        resultSearchController.hidesNavigationBarDuringPresentation = false
        resultSearchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        self.searchBarPlaceHolder.addSubview(searchBar)
        
        
        let constraints: [NSLayoutConstraint] = [
            searchBar.topAnchor.constraint(equalTo: searchBarPlaceHolder.topAnchor),
            searchBar.leadingAnchor.constraint(equalTo: searchBarPlaceHolder.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: searchBarPlaceHolder.trailingAnchor),
            searchBar.bottomAnchor.constraint(equalTo: searchBarPlaceHolder.bottomAnchor),
        ]
        NSLayoutConstraint.activate(constraints)
        
        
        print(cityAnimation.cityFirstAnimation[0])
        print(cityAnimation.cityFirstAnimation[1])
        
        let currentLocation = CLLocationCoordinate2DMake(-37.817649, 144.959216)
        let span = MKCoordinateSpan(latitudeDelta: 0.04, longitudeDelta: 0.04)
        let region = MKCoordinateRegion(center: currentLocation, span: span)
        mapView.setRegion(region, animated: true)
        
//        mapView.mapType
        mapView.delegate = self
//        addOneRoute()
//        addTwoRoute()
//        addThreeRoute()
        addRoute()
    }
    
    func addRoute() {
        
        var start = 0
        while start < 152 {
            let location1: CLLocationCoordinate2D = CLLocation(latitude: cityAnimation.cityFirstAnimation[start], longitude: cityAnimation.cityFirstAnimation[start+1]).coordinate
            let location2: CLLocationCoordinate2D = CLLocation(latitude: cityAnimation.citySecondAnimation[start], longitude: cityAnimation.citySecondAnimation[start+1]).coordinate
            var lineTitle = "\(start)"
            addPolyLineToMap(coordinates: [location1, location2], title: lineTitle)
            start += 2
        }
    }
    
    
    func addOneRoute() {
        let location1: CLLocationCoordinate2D = CLLocation(latitude: -37.813283, longitude: 144.973973).coordinate
        let location2: CLLocationCoordinate2D = CLLocation(latitude: -37.817602, longitude: 144.959096).coordinate
        var lineTitle = "1"
        addPolyLineToMap(coordinates: [location1, location2], title: lineTitle)
    }
    func addTwoRoute() {
        let location1: CLLocationCoordinate2D = CLLocation(latitude: -37.813310, longitude: 144.951438).coordinate
        let location2: CLLocationCoordinate2D = CLLocation(latitude: -37.812462, longitude: 144.953883).coordinate
        var lineTtitle = "2"
        addPolyLineToMap(coordinates: [location1, location2], title: lineTtitle)
    }
    func addThreeRoute() {
        let location1: CLLocationCoordinate2D = CLLocation(latitude: -37.813310, longitude: 144.951438).coordinate
        let location2: CLLocationCoordinate2D = CLLocation(latitude: -37.815141, longitude: 144.952296).coordinate
        var lineTtitle = "3"
        addPolyLineToMap(coordinates: [location1, location2], title: lineTtitle)
    }
    
    
    func addPolyLineToMap(coordinates: [CLLocationCoordinate2D],title: String){

    let geodesic = MKGeodesicPolyline(coordinates: coordinates, count: coordinates.count)
    geodesic.title = title
    dict["1"] = 1
    dict["2"] = 0.5
    dict["3"] = 0.2
    
//    geodesic.securityLevel = 0.5
    mapView.addOverlay(geodesic)
    
}
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let polyline = overlay as? MKGeodesicPolyline {
            let pr = MKPolylineRenderer(overlay: polyline);
            let securityLevel = dict[polyline.title ?? ""] ?? 0.0
            let randomSafeLevel = CGFloat.random(in: 0 ..< 1)
            pr.strokeColor = UIColor.blue.withAlphaComponent(randomSafeLevel)
//            pr.strokeColor = UIColor.blue.withAlphaComponent(CGFloat(securityLevel))
            pr.lineWidth = 3;
            return pr
        }
        if let polyline = overlay as? MKPolyline {
            let renderer = MKPolylineRenderer(polyline: polyline)
            renderer.strokeColor = UIColor.red
            return renderer
        }
        return MKOverlayRenderer()
    }
}

extension CityLoactionViewController: LocationSearchTableProtocol {
    func locationSearchTableDidSelect(data: MKPlacemark) {
        if let route = self.routePolyLine, let annotation = self.annotation {
            self.mapView.removeOverlay(route)
            self.mapView.removeAnnotation(annotation)
        }
        self.annotation = data
        self.mapView.addAnnotation(data)
        let request = MKDirections.Request()
        //current location
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: -37.813283, longitude: 144.973973), addressDictionary: nil))
        request.destination = MKMapItem(placemark: data)
        request.requestsAlternateRoutes = false
        request.transportType = .walking
        
        let directions = MKDirections(request: request)
        
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            if let route = unwrappedResponse.routes.first {
                self.routePolyLine = route.polyline
                
                self.mapView.addOverlay(route.polyline)
                self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
            }
        }    }
}
