//
//  LoadingView.swift
//  VicGuardian
//
//  Created by sai ma on 8/5/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import UIKit

private var ag_loadingIndicatorAssociationKey: Int = 0
private var ag_loadingIndicatorContainerAssociationKey: Int = 0
extension UIView {
    private var ag_loadingIndicator: AutogateAnimatedLogoView? {
        get {
            return objc_getAssociatedObject(self, &ag_loadingIndicatorAssociationKey) as? AutogateAnimatedLogoView
        }
        set {
            objc_setAssociatedObject(self, &ag_loadingIndicatorAssociationKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    private var ag_loadingIndicatorContainer: UIView? {
        get {
            return objc_getAssociatedObject(self, &ag_loadingIndicatorContainerAssociationKey) as? UIView
        }
        set {
            objc_setAssociatedObject(self, &ag_loadingIndicatorContainerAssociationKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func showLoadingIndicator(disableUserInteraction: Bool = true, otherUserInteractionDisabledViews: [UIView] = []) {
        if disableUserInteraction {
            self.isUserInteractionEnabled = false
        }
        
        if let loadingIndicator = self.ag_loadingIndicator {
            loadingIndicator.userInteractionDisabledViews = otherUserInteractionDisabledViews
            return
        }
        
        let loadingIndicatorView: AutogateAnimatedLogoView = {
            let _loadingIndicatorView = AutogateAnimatedLogoView()
            _loadingIndicatorView.userInteractionDisabledViews = otherUserInteractionDisabledViews
            self.ag_loadingIndicator = _loadingIndicatorView
            return _loadingIndicatorView
        }()
        
        loadingIndicatorView.startAnimating()
        
        let containerView = UIView()
        containerView.backgroundColor = .white
        self.ag_loadingIndicatorContainer = containerView
        self.addSubview(containerView)
        containerView.addSubview(loadingIndicatorView)
        
        loadingIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            containerView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            containerView.topAnchor.constraint(equalTo: self.topAnchor),
            containerView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            loadingIndicatorView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            loadingIndicatorView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)
            ])
    }
    
    func hideLoadingIndicator() {
        self.isUserInteractionEnabled = true
        ag_loadingIndicator?.userInteractionDisabledViews = []
        ag_loadingIndicator?.removeFromSuperview()
        ag_loadingIndicator = nil
        ag_loadingIndicatorContainer?.removeFromSuperview()
        ag_loadingIndicatorContainer = nil
    }
}
