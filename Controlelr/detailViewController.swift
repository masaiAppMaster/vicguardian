//
//  detailViewController.swift
//  VicGuardian
//
//  Created by erica12580 on 27/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import CoreLocation
import Firebase
import GooglePlaces

class detailViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var titleText: UITextField!
    
    @IBOutlet weak var descriptionText: UITextField!
    
    @IBOutlet weak var latText: UITextField!
    
    @IBOutlet weak var longText: UITextField!
    
    @IBOutlet weak var addressText: UITextView!
    
    @IBOutlet weak var openStatusText: UITextField!
    
    @IBOutlet weak var photoImage: UIImageView!
    
    @IBOutlet weak var photoCredit: UILabel!
    
    
    var locationManager: CLLocationManager = CLLocationManager()
    var currentLocation: CLLocationCoordinate2D?
    var passedSelectedAnnotation: MKAnnotation?
    var ref: DatabaseReference!
    var userDataManager: UserDataManager!
    var formattedAddress: String!
    var placeID: String!
    var placesClient: GMSPlacesClient!
    var formatAddress: String!
    // 0 = false, 1 = true, 3 = not sure
    var opening: Int!
    var photoRef: String!
    let dispatchGroup = DispatchGroup()
    var html_attributes: String!
    
    required init?(coder aDecoder: NSCoder) {
        self.opening = 3
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userDataManager = getAppDelegate().app.userDataManager
        placesClient = GMSPlacesClient()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter = 10
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        latText.isUserInteractionEnabled = false
        longText.isUserInteractionEnabled = false
        openStatusText.isUserInteractionEnabled = false
        let borderColor = UIColor(red: 204.0 / 255.0, green: 204.0 / 255.0, blue: 204.0 / 255.0, alpha: 1.0)
        
        addressText.layer.borderColor = borderColor.cgColor
        addressText.layer.borderWidth = 1.0
        addressText.layer.cornerRadius = 5.0
        photoCredit.adjustsFontSizeToFitWidth = true
        
        if passedSelectedAnnotation != nil {
            titleText.text = (passedSelectedAnnotation?.title)!
            descriptionText.text = (passedSelectedAnnotation?.subtitle)!
            latText.text = "\(passedSelectedAnnotation!.coordinate.latitude)"
            longText.text = "\(passedSelectedAnnotation!.coordinate.longitude)"
            //            getGoogleAdrress(location: passedSelectedAnnotation!)
            
            dispatchGroup.enter()
            getPlaceInfo(location: passedSelectedAnnotation!)
            dispatchGroup.notify(queue: .main) {
                self.addressText.text = self.formattedAddress
                if let credit = self.html_attributes {
                    self.photoCredit.text = "Photo Credit: " + credit
                }
                
                if self.opening == 0 {
                    self.openStatusText.text = "CLOSED"
                }
                else if self.opening == 1 {
                    self.openStatusText.text = "OPENING"
                } else {
                    self.openStatusText.text = "NOT SURE"
                }
                
                self.getPhotoImage()
            }
            
        }
        
        addTapToHideKeyboardGesture()
    }
    
    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations:
        [CLLocation])
    {
        let loc: CLLocation = locations.last!
        currentLocation = loc.coordinate
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func saveLocation(_ sender: Any) {
        if self.titleText.text!.trimmingCharacters(in: .whitespaces).isEmpty {
            let alert = UIAlertController(title: "Illegal title input!", message: "Title should not be blank.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        }
        if self.latText.text == "" || self.longText.text == "" {
            let alert = UIAlertController(title: "Illegal latitude or longitude input!", message: "Latitude or longitude should not be nil.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        }
        
        
        let annotation: LocationAnnotation = LocationAnnotation(newTitle: self.titleText.text!,newSubtitle: self.descriptionText.text!, lat: Double(self.latText.text!)!, long: Double(self.longText.text!)!)
        ref = Database.database().reference().child("savedPlaces").child(userDataManager.uid).childByAutoId()
        ref.setValue([
            "title": annotation.title,
            "subtitle": annotation.subtitle,
            "lat": annotation.coordinate.latitude,
            "long": annotation.coordinate.longitude
            ])
        let alert = UIAlertController(title: "Location successfully saved!", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: popViewHandler(alert:)))
        self.present(alert, animated: true)
        
    }
    
    func popViewHandler(alert: UIAlertAction!){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //    @IBAction func insertCurrentLocation(_ sender: Any) {
    //        if currentLocation == nil {
    //            currentLocation = CLLocationCoordinate2DMake(-37.877512, 145.044216)
    //        }
    //        latText.text = "\(currentLocation!.latitude)"
    //        longText.text = "\(currentLocation!.longitude)"
    //    }
    
    func addTapToHideKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(tap:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(tap: UITapGestureRecognizer){
        self.titleText.endEditing(true)
        self.descriptionText.endEditing(true)
        self.addressText.endEditing(true)
    }
    
    // Get place information from latitude and longitude.
    func getPlaceInfo(location: MKAnnotation) {
        var lookUpStr = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=\(location.title!!)&inputtype=textquery&fields=photos,formatted_address,name,opening_hours,permanently_closed&locationbias=circle:100@\(location.coordinate.latitude),\(location.coordinate.longitude)&key=AIzaSyCniR4gRiVeDqK8NG6gGQnAiQ5JH2N9wvI"
        
        lookUpStr = lookUpStr.replacingOccurrences(of: " ", with: "+")
        
        var jsonDict: [AnyHashable : Any]? = nil
        let url = URL(string: lookUpStr)
        let session = URLSession.shared
        if let usableUrl = url {
            let task = session.dataTask(with: usableUrl, completionHandler: { (data, response, error) in
                if let data = data {
                    do {
                        jsonDict = try JSONSerialization.jsonObject(with: data, options: []) as? [AnyHashable : Any]
                    } catch {
                        print(error)
                    }
                    
                    let jsonResults = jsonDict?["candidates"] as? Array<Any>
                    let addressResult = jsonResults?.first as? Dictionary<String, Any>
                    self.formattedAddress = addressResult?["formatted_address"] as! String
                    let photos = addressResult?["photos"] as? Array<Any>
                    if let photo = photos?.first as? Dictionary<String, Any>{
                        self.photoRef = photo["photo_reference"] as! String
                        if let html_array = photo["html_attributions"] as? Array<Any> {
                            self.html_attributes = html_array.first as! String
                        }
                    }
                    // Get whether it is open.
                    if let openingHours = addressResult?["opening_hours"] as? Dictionary<String, Any>{
                        if let openStr = openingHours["open_now"] as? Int{
                            if openStr == 1 || openStr == 0{
                                self.opening = openStr
                            }
                            else{
                                self.opening = 3
                            }
                        }
                    }
                    self.dispatchGroup.leave()
                }
            })
            task.resume()
        }
    }
    
    // Get place image from photo reference.
    func getPhotoImage() {
        if let photoRef = photoRef {
            var lookUpStr = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=300&photoreference=\(photoRef)&key=AIzaSyCniR4gRiVeDqK8NG6gGQnAiQ5JH2N9wvI"
            
            lookUpStr = lookUpStr.replacingOccurrences(of: " ", with: "+")
            
            let catPictureURL = URL(string: lookUpStr)!
            
            // Creating a session object with the default configuration.
            // You can read more about it here https://developer.apple.com/reference/foundation/urlsessionconfiguration
            let session = URLSession(configuration: .default)
            
            // Define a download task. The download task will download the contents of the URL as a Data object and then you can do what you wish with that data.
            let downloadPicTask = session.dataTask(with: catPictureURL) { (data, response, error) in
                // The download has finished.
                if let e = error {
                    print("Error downloading cat picture: \(e)")
                } else {
                    // No errors found.
                    // It would be weird if we didn't have a response, so check for that too.
                    if let res = response as? HTTPURLResponse {
                        print("Response code \(res.statusCode)")
                        if let imageData = data {
                            // Finally convert that Data into an image and do what you wish with it.
                            let image = UIImage(data: imageData)
                            DispatchQueue.main.async { // Make sure you're on the main thread here
                                //Resize the image according to ratio
                                let containerView = UIView(frame: CGRect(x:0,y:0,width:220,height:100))
                                
                                if let image = image {
                                    let ratio = image.size.width / image.size.height
                                    if containerView.frame.width > containerView.frame.height {
                                        let newHeight = containerView.frame.width / ratio
                                        self.photoImage.frame.size = CGSize(width: containerView.frame.width, height: newHeight)
                                    }
                                    else{
                                        let newWidth = containerView.frame.height * ratio
                                        self.photoImage.frame.size = CGSize(width: newWidth, height: containerView.frame.height)
                                    }
                                }
                                
                                self.photoImage.image = image
                            }
                        } else {
                            print("Couldn't get image: Image is nil")
                        }
                    } else {
                        print("Couldn't get response code for some reason")
                    }
                }
            }
            
            downloadPicTask.resume()
        }
    }
    
    //    func getPlaceDetail() {
    //        // Specify the place data types to return.
    //        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.phoneNumber.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue) |
    //            UInt(GMSPlaceField.openingHours.rawValue) |
    //            UInt(GMSPlaceField.photos.rawValue))!
    //
    //        placesClient?.fetchPlace(fromPlaceID: placeID, placeFields: fields, sessionToken: nil, callback: {
    //            (place: GMSPlace?, error: Error?) in
    //            if let error = error {
    //                print("An error occurred: \(error.localizedDescription)")
    //                return
    //            }
    //            if let place = place {
    //                self.formatAddress = place.formattedAddress
    //                self.phoneNumber = place.phoneNumber
    //                self.openingHours = place.openingHours
    //                self.photoRef = place.photos
    //                print("The selected place is: \(place.name)")
    //            }
    //        })
    //    }
}
