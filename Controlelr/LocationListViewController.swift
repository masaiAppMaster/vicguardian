//
//  LocationListViewController.swift
//  VicGuardian
//
//  Created by erica12580 on 17/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation
import UIKit
import Firebase

protocol LocationListViewControllerDelegate{
    
    func didSelectLocation (_ annotation: LocationAnnotation)
}

class LocationListViewController: UITableViewController{
    
    @objc var locationList: NSMutableArray
    @objc var mapViewController: MapViewController?
    var delegate: LocationListViewControllerDelegate?
    var ref: DatabaseReference!
    var userDataManager: UserDataManager!
    var keyArray: Array<Any>
    
    required init?(coder aDecoder: NSCoder) {
        locationList = NSMutableArray()
        keyArray = Array()
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userDataManager = getAppDelegate().app.userDataManager
        loadData()
    }
    
    func loadData() {
        ref = Database.database().reference().child("savedPlaces").child(userDataManager.uid)
        ref.observe(DataEventType.value) { snapShot in
            let enumerator = snapShot.children
            var array: [LocationAnnotation] = []
            while let snapShotValue = enumerator.nextObject() as? DataSnapshot {
                let dict = snapShotValue.value as? NSDictionary
                let key = snapShotValue.key
                let lat = dict?["lat"] as! Double
                let long = dict?["long"] as! Double
                let title = dict?["title"] as! String
                let subTitle = dict?["subtitle"] as! String
                let locAn = LocationAnnotation(newTitle: title, newSubtitle: subTitle, lat: lat, long: long)
                array.append(locAn)
                self.keyArray.append(key)
            }
            self.locationList.removeAllObjects()
            for locAn in array{
                self.locationList.add(locAn)
            }
            self.tableView.reloadData()
        }
    }
    
    //    @objc func didSaveLocation(_ annotation: LocationAnnotation) {
    //        ref = Database.database().reference().child("savedPlaces").child(userDataManager.uid).childByAutoId()
    //        ref.setValue([
    //            "title": annotation.title,
    //            "subtitle": annotation.subtitle,
    //            "lat": annotation.coordinate.latitude,
    //            "long": annotation.coordinate.longitude
    //            ])
    //        loadData()
    //    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //        let mapVC = storyBoard.instantiateViewController(withIdentifier: "mapViewController") as! MapViewController
        
        delegate?.didSelectLocation(locationList.object(at: indexPath.row) as!
            LocationAnnotation)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Deleted")
            
            ref = Database.database().reference().child("savedPlaces").child(userDataManager.uid).child(keyArray[indexPath.row] as! String)
            ref.removeValue()
            self.keyArray.remove(at: indexPath.row)
            self.locationList.removeObject(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //        if segue.identifier == "addLocationSegue"{
        //            let controller = segue.destination as! NewLocationViewController
        //            controller.delegate = self
        //        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.locationList.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let locAn: LocationAnnotation = self.locationList.object(at: indexPath.row) as! LocationAnnotation
        
        cell.textLabel!.text = locAn.title
        cell.detailTextLabel!.text = locAn.subtitle
        
        return cell
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
