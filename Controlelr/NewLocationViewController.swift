//
//  NewLocationViewController.swift
//  VicGuardian
//
//  Created by erica12580 on 17/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit
import Firebase


class NewLocationViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var titleText: UITextField!
    
    @IBOutlet weak var descriptionText: UITextField!
    
    @IBOutlet weak var latText: UITextField!
    
    @IBOutlet weak var longText: UITextField!
    
    
    var locationManager: CLLocationManager = CLLocationManager()
    var currentLocation: CLLocationCoordinate2D?
    var passedSelectedAnnotation: MKAnnotation?
    var ref: DatabaseReference!
    var userDataManager: UserDataManager!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userDataManager = getAppDelegate().app.userDataManager
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter = 10
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        if passedSelectedAnnotation != nil {
            titleText.text = (passedSelectedAnnotation?.title)!
            descriptionText.text = (passedSelectedAnnotation?.subtitle)!
            latText.text = "\(passedSelectedAnnotation!.coordinate.latitude)"
            longText.text = "\(passedSelectedAnnotation!.coordinate.longitude)"
        }
    }
    
    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations:
        [CLLocation])
    {
        let loc: CLLocation = locations.last!
        currentLocation = loc.coordinate
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func insertCurrentLocation(_ sender: Any) {
        if currentLocation == nil {
            currentLocation = CLLocationCoordinate2DMake(-37.877512, 145.044216)
        }
        latText.text = "\(currentLocation!.latitude)"
        longText.text = "\(currentLocation!.longitude)"
    }
    
    @IBAction func saveLocation(_ sender: Any) {
        // Validation against user input.
        if self.titleText.text!.trimmingCharacters(in: .whitespaces).isEmpty {
            let alert = UIAlertController(title: "Illegal title input!", message: "Title should not be blank.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        }
        if self.latText.text == "" || self.longText.text == "" {
            let alert = UIAlertController(title: "Illegal latitude or longitude input!", message: "Latitude or longitude should not be nil.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        }
        if Double(self.latText.text!)! < -43.00311 || Double(self.latText.text!)! > -12.46113 {
            let alert = UIAlertController(title: "Illegal latitude input!", message: "Latitude input should range from -43.00311 to -12.46113.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        }
        if Double(self.longText.text!)! < 113.6594 || Double(self.longText.text!)! > 153.61194 {
            let alert = UIAlertController(title: "Illegal longitude input!", message: "Latitude input should range from 113.6594 to 153.61194.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        }
        
        
        let annotation: LocationAnnotation = LocationAnnotation(newTitle: self.titleText.text!,newSubtitle: self.descriptionText.text!, lat: Double(self.latText.text!)!, long: Double(self.longText.text!)!)
        ref = Database.database().reference().child("savedPlaces").child(userDataManager.uid).childByAutoId()
        ref.setValue([
            "title": annotation.title,
            "subtitle": annotation.subtitle,
            "lat": annotation.coordinate.latitude,
            "long": annotation.coordinate.longitude
            ])
        let alert = UIAlertController(title: "Location successfully saved!", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: popViewHandler(alert:)))
        self.present(alert, animated: true)
        
    }
    
    func popViewHandler(alert: UIAlertAction!){
        //        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //        let newViewController = storyBoard.instantiateViewController(withIdentifier: "locationListViewController") as! LocationListViewController
        //        self.present(newViewController, animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
}
