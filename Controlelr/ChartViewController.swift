
import UIKit
import XJYChart


class ChartViewController: UIViewController, XJYChartDelegate, SurburbTableViewControllerProtocl {
    func surburbTableViewControllerProtocl(didSelect suburb: String) {
        self.selectedSuburb = suburb
        self.showSuburbLabel.text = suburb
        findNumberFromCriminalData()
        findLinerChartDataFormCriminalData()
        setChartData()
//        findChartTopNumber()
        setLinerChartData()
//        lineChartContainer.isHidden = false
//        if self.showSuburbLabel.text == "AIRLY" {
//            self.imageLineChart.image = UIImage(named: "chart.png")        }else if  self.showSuburbLabel.text == "CAULFIELD" {
//            self.imageLineChart.image = UIImage(named: "chart2.png")
//        }
    }
    var criminalData: [CriminalData] = []
    var criminalYearData: [CriminalSuburbYearCountData] = []
    private var selectedSuburb: String?
    //   @IBOutlet weak var barChartView: !
//    @IBOutlet weak var barChartView: XBarChart!
    @IBOutlet weak var barChartContainer: UIView!
    @IBOutlet weak var lineChartContainer: UIView!
    
    @IBOutlet weak var imageLineChart: UIImageView!
    
    @IBOutlet weak var showSuburbLabel: UILabel!
    var showFristItem: XBarItem? = nil
    var showSecondItem: XBarItem? = nil
    var showThirdItem: XBarItem? = nil
    var showFourthItem: XBarItem? = nil
    var crimainalDataChart: [CriminalSingleData] = []
    var crimainalDataLineChart: [CriminalSingleData] = []
    var itemArray: NSMutableArray = []
    @IBOutlet weak var showSafeLabel: UILabel!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
//        lineChartContainer.isHidden = true
        criminalData = getAppDelegate().app.criminalDataManager.array
//        self.barChartView.
//        self.barChartView.data = [2 , 3, 3]
//        self.barChartView.barColors = [UIColor.red, UIColor.blue, UIColor.gray, UIColor.blue]
//        self.barChartView.xLabels = ["a", "b", "c"]
//        let item1 = XBarItem.init(dataNumber: 10, color: .red, dataDescribe: "1")
//        let item2 = XBarItem.init(dataNumber: 5, color: .blue, dataDescribe: "2")
//        let array: NSMutableArray = [item1, item2]
//
//        let config = XBarChartConfiguration()
//        config.isScrollable = true
//        config.x_width = 20.0
//        let size = barChartContainer.bounds
//        let barChart1 = XBarChart.init(frame: CGRect(x: 0,y: 0,width: size.width, height: size.height), dataItemArray: array, topNumber: 100, bottomNumber: 0, chartConfiguration: config)
//        self.barChartContainer.addSubview(barChart1!)
        
        
    }
    
    func findNumberFromCriminalData() {
        self.itemArray = []
        crimainalDataChart = []
        for singleCriminalData in self.criminalData {
            if singleCriminalData.suburb == selectedSuburb && singleCriminalData.year == 2016 {
                let crimianlSingleDataChart = CriminalSingleData()
                let item = XBarItem.init(dataNumber: singleCriminalData.count as NSNumber, color: .red, dataDescribe: singleCriminalData.kind)
                itemArray.add(item)
                crimianlSingleDataChart.kind = singleCriminalData.kind
                crimianlSingleDataChart.count = singleCriminalData.count
                self.crimainalDataChart.append(crimianlSingleDataChart)
            }
        }
    }
    
    func findTopNumber() -> Int{
        let greatestNumber = self.crimainalDataChart.max(by: {$0.count < $1.count})
        greatestNumber?.count == nil ? showSafe() : nil
        return greatestNumber?.count ?? 0
    }
    
    func showSafe() {
//        showSafeLabel.text = "No crime, Possible Safety"
        print("No crime, Possible Safety")
    }
    
    func setChartData() {
        
        let config = XBarChartConfiguration()
        config.isScrollable = true
        config.x_width = 20.0
        let size = barChartContainer.bounds
        let barChart1 = XBarChart.init(frame: CGRect(x: 0,y: 0,width: size.width, height: size.height), dataItemArray: self.itemArray, topNumber: findTopNumber() as NSNumber, bottomNumber: 0, chartConfiguration: config)
        self.barChartContainer.addSubview(barChart1!)
        
    }
    
    
    
//                    showFristItem = XBarItem.init(dataNumber: singleCriminalData.count as NSNumber, color: <#T##UIColor!#>, dataDescribe: singleCriminalData.kind)
    
    func userClickedOnBarAtIndex(barIndex: NSInteger) {
        print(barIndex)
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSuburb" {
            if let destVC = segue.destination as? SurburbTableViewController {
                //                destVC.selectedSuburb = selectedSuburb
                destVC.delegate = self
            }
        }
    }
    
    func findChartDataFromCrimialData() {
        
        crimainalDataChart = []
        for singleCriminalData in self.criminalData {
            if singleCriminalData.suburb == selectedSuburb && singleCriminalData.year == 2016 {
                let crimianlSingleDataChart = CriminalSingleData()
                let item = XBarItem.init(dataNumber: singleCriminalData.count as NSNumber, color: .red, dataDescribe: singleCriminalData.kind)
                itemArray.add(item)
                crimianlSingleDataChart.kind = singleCriminalData.kind
                crimianlSingleDataChart.count = singleCriminalData.count
                self.crimainalDataChart.append(crimianlSingleDataChart)
            }
        }
    }
    
    func findLinerChartDataFormCriminalData() {
        criminalYearData = []
        addOneYear(year: 2012)
        addOneYear(year: 2013)
        addOneYear(year: 2014)
        addOneYear(year: 2015)
        addOneYear(year: 2016)
    }
    
    func addOneYear(year: Int) {
        var tototalOneYear = 0
        for singleCriminalData in self.criminalData {
            if singleCriminalData.suburb == selectedSuburb && singleCriminalData.year == year{
                tototalOneYear += singleCriminalData.count
            }
        }
//        CriminalSuburbYearCountData
        let singleCriminalYearData = CriminalSuburbYearCountData()
        singleCriminalYearData.year = year
        singleCriminalYearData.count = tototalOneYear
        self.criminalYearData.append(singleCriminalYearData)
    }
    
    
    func setLinerChartData() {
        let numberArray: NSMutableArray = [self.findDataDiscribe(year: 2012), self.findDataDiscribe(year: 2013), self.findDataDiscribe(year: 2014), self.findDataDiscribe(year: 2015), self.findDataDiscribe(year: 2016)]
        
        let lineChartItem = XLineChartItem(dataNumber: numberArray, color: UIColor.red)
        
        let config = XNormalLineChartConfiguration()
        config.lineMode = .CurveLine
        
        
        let size = lineChartContainer.bounds
        let lineChart = XLineChart(frame: CGRect(x: 0,y: 0,width: size.width, height: size.height),
            dataItemArray: [lineChartItem!],
            dataDiscribeArray: ["2012", "2013", "2014", "2015", "2016"],
            topNumber: self.findChartTopNumber()+1 as NSNumber,
            bottomNumber: 0,
            graphMode: .MutiLineGraph,
            chartConfiguration: config)
        self.lineChartContainer.addSubview(lineChart!)
        
    }
    func findDataDiscribe(year: Int) -> Int {
        var initNumber = 0
        for single in self.criminalYearData{
            if single.year == year{
                initNumber = single.count
            }
        }
        return initNumber
    }
    func findChartTopNumber() -> Int{
        let greatestNumber = self.criminalYearData.max(by: {$0.count < $1.count})
        greatestNumber?.count == nil ? showSafe() : nil
        return greatestNumber?.count ?? 0
    }
    
}
