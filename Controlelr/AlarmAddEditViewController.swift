//
//  AlarmAddEditViewController.swift
//  VicGuardian
//
//  Created by sai ma on 16/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import UIKit
import MapKit
//protocol AlarmAddEditViewControllerProtocol: class {
//    func alarmAddEditViewControllerDidSet(time: Double, message: String)
//}


class AlarmAddEditViewController: UIViewController, LocationSearchTableProtocol,UIPickerViewDelegate,UIPickerViewDataSource {

    
    func locationSearchTableDidSelect(data: MKPlacemark) {
        receiveMKData = data
        getRouterTime()
        
    }
    
    @IBOutlet weak var dataPicker: UIDatePicker!
//    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageLabel: UITextField!
    var time: TimeInterval = 0.0
//    weak var delegate: AlarmAddEditViewControllerProtocol?
    var messgaeInfo = ""
    var resultSearchController: UISearchController!
    var receiveMKData: MKPlacemark?
    var alarmLocationTime: LocationAnnotation!
    var locationManager: LocationManager!
    @IBOutlet weak var showExpectTime: UILabel!
    
    @IBOutlet weak var timePick: UIPickerView!
    
    @IBOutlet weak var minsPick: UIPickerView!
    var searchTime: Double = 0
    var hourData: [String] = []
    var totalTime: Int = 0
    override func viewDidLoad() {
        
        super.viewDidLoad()
        addTapToHideKeyboardGesture()
        hourData = setTimePicker(numberLimit: 61)
        timePick.dataSource = self
        minsPick.dataSource = self
        timePick.delegate = self
        minsPick.delegate = self
//        dataPick
        locationManager = getAppDelegate().app.locationManager
        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        locationSearchTable.delegate = self
        resultSearchController.searchResultsUpdater = locationSearchTable
        let searchBar = resultSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Input your destination, to get time"
        navigationItem.titleView = resultSearchController?.searchBar
        resultSearchController.hidesNavigationBarDuringPresentation = false
        resultSearchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        
      
    }

    func setTimePicker(numberLimit: Int) -> [String] {
        var i = 1
        var hourDatas: [String] = ["0"]
        while i < numberLimit {
            hourDatas.append("\(i)")
            i = i + 1
        }
        return hourDatas
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return hourData[row]
    }
    
    func getRouterTime() {
        
        alarmLocationTime = LocationAnnotation(newTitle: "alarm location", newSubtitle: "", lat: receiveMKData!.coordinate.latitude, long: receiveMKData!.coordinate.longitude)
        let request = MKDirections.Request()
        let sourceP = CLLocationCoordinate2DMake((locationManager.currentLocation.latitude), (locationManager.currentLocation.longitude))
        let desP = CLLocationCoordinate2DMake(alarmLocationTime.coordinate.latitude, alarmLocationTime.coordinate.longitude)
        let destination = MKPlacemark(coordinate: desP)
        let source = MKPlacemark(coordinate: sourceP)
        request.source = MKMapItem(placemark: source)
        request.destination = MKMapItem(placemark: destination)
        request.transportType = MKDirectionsTransportType.walking
        request.requestsAlternateRoutes = true
        let directions = MKDirections(request: request)
        directions.calculate { (response, error) in
            if var route = response?.routes {
                route.sort(by: {$0.expectedTravelTime < $1.expectedTravelTime})
                let quickestRoute: MKRoute = route[0]
                self.searchTime = quickestRoute.expectedTravelTime
                let StringTime = self.getString(from: quickestRoute.expectedTravelTime)
                self.showExpectTime.text = StringTime
//                showExpectTime.getString
                
            }
        }
    }
    
    func getString(from time: TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format: "%02i:%02i", hours,minutes)
    }
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "alarmSearchData" {
//            if let destVC = segue.destination as? LocationSearchTable {
//                destVC.delegate = self
//            }
//        }
//    }
 
//    func numberOfSections(in tableView: UITableView) -> Int {
//        // Return the number of sections.
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell : SetMessageCell
//        cell = tableView.dequeueReusableCell(withIdentifier: "setMessageCell", for: indexPath) as! SetMessageCell
//        messgaeInfo = cell.MessageLabel.text!
//        return cell
//    }
    
    
    @IBAction func saveAlarmData(_ sender: Any) {
        time = TimeInterval(allTime)
//        messgaeInfo = messageLabel.text ?? "Nothing"
        if messageLabel.text == "" {
            messgaeInfo = "No extra message, please contact your protectee"
        } else {
            messgaeInfo = messageLabel.text ?? "No extra information"
        }
        if time != 0.0 || searchTime != 0.0 {
            if time == 0.0 {
                time = time + searchTime
            }
            let changeTime = Int(time) / 60
            time = TimeInterval(changeTime * 60)
//            delegate?.alarmAddEditViewControllerDidSet(time: time, message: messgaeInfo)
//            self.navigationController?.popViewController(animated: true)
//            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let newViewController = storyBoard.instantiateViewController(withIdentifier: "showTIme")
//            self.present(newViewController, animated: true, completion: nil)
//            performSegue(withIdentifier: "afterEditShowTImeSegue", sender: time)
//            performSegue(withIdentifier: "afterEditShowTImeSegue", sender: messgaeInfo)
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "showTIme") as! MainAlarmViewController
            vc.time = time
            
            messgaeInfo = messgaeInfo + "\n Guardian timer runs up and I have not reached my desitination: \(receiveMKData?.title ?? "unknown")! \n My location: \nLatitude: \(locationManager.currentLocation.latitude) \nLongitude: \(locationManager.currentLocation.longitude)"
            vc.messageInfo = messgaeInfo
            TimerManager.start(withSeconds: Int(time))
//            self.navigationController?.popToViewController(vc, animated: true)
            
//            self.navigationController?.pushViewController(vc, animated: true)
            self.navigationController?.setViewControllers([vc], animated: true)
//            self.navigationController?.hidesBarsOnTap = true
//            self.pushViewController(vc, animated: true)
        }
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if(segue.identifier == "afterEditShowTImeSegue"){
//            let displayVC = segue.destination as! MainAlarmViewController
//            displayVC.time = time
//            displayVC.messageInfo = self.messgaeInfo
//            TimerManager.start(withSeconds: Int(time))
//            
////            displayVC.time = inputLabel.text!
//        }
//    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return hourData.count
    }
    var setHourTime = 0
    var setMinsTime = 0
    var allTime = 0
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        print(hourData[row])
        if pickerView.tag == 1 {
            setHourTime = Int(hourData[row])!*3600
        } else {
            setMinsTime = Int(hourData[row])!*60
        }
        allTime = 0
        allTime = setHourTime + setMinsTime + Int(searchTime)
        self.showExpectTime.text = self.getString(from: TimeInterval(allTime))
//        timePick.selectedRow(inComponent: 1)
    }
    
    
    func addTapToHideKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(tap:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(tap: UITapGestureRecognizer) {
//        self.email.endEditing(true)
//        self.password.endEditing(true)
        self.messageLabel.endEditing(true)
    }
    
}



//
//class SetMessageCell: UITableViewCell {
//    @IBOutlet weak var MessageLabel: UITextField!
//
//}
