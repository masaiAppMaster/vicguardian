//
//  AddRelationshipViewController.swift
//  VicGuardian
//
//  Created by sai ma on 4/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import UIKit
import Firebase

class AddRelationshipViewControllerFromMessage: UIViewController {
    var userDataManager: UserDataManager!
    @IBOutlet weak var addEmailLabel: UITextField!
    var hasRelationship = false
    var hasProtectorRef: DatabaseReference!
    var protectID: String? = nil
    var singleUserRealtionship = Relationship()
    var addRef: DatabaseReference!
    var app: App!
    
    override func viewDidLoad() {
        super .viewDidLoad()
        userDataManager = getAppDelegate().app.userDataManager
        addTapToHideKeyboardGesture()
    }
    
    
    @IBAction func addRelationButton(_ sender: Any) {
        self.app = getAppDelegate().app
        userDataManager = getAppDelegate().app.userDataManager
        if addEmailLabel.text == nil{
            wrongEmail()
        } else {
            checkHasEmail()
            
            if protectID == nil {
                noUserAlert()
            }else {
                if protectID == userDataManager.helperID{
                    sameProtecteeIDNotification()
                }else {
                    findProtectorID()
                    userDataManager = getAppDelegate().app.userDataManager
                    singleUserRealtionship.hashelper ? fullHelperAlert() : addUsersRelationship()
                }
            }
        }
    }
    
    
    
    func checkHasEmail() {
        for user in userDataManager.users {
            guard let addEmailLabel = self.addEmailLabel.text else { return }
            hasRelationship = false
            user.email == addEmailLabel ? self.hasRelationship = true : nil
            hasRelationship ? self.protectID = user.id : nil
        }
    }
    
    func findProtectorID() {
        for singleRaktionship in userDataManager.relationships {
            singleRaktionship.myID == self.protectID! ? self.singleUserRealtionship = singleRaktionship : nil
            
        }
    }
    
    func sameProtecteeIDNotification() {
        let alert = UIAlertController(title: "Notification", message: "This user has been your protectee, please enter new user, Or contact this user to delete your protect relationship ", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func wrongEmail() {
        let alert = UIAlertController(title: "Error", message: "Please input email", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func noUserAlert() {
        let alert = UIAlertController(title: "Error", message: "Incorrect user", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func fullHelperAlert() {
        let alert = UIAlertController(title: "Error", message: "Cann't add this user and this user has helper", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func addUsersRelationship() {
        userDataManager = getAppDelegate().app.userDataManager
        let addRef = Database.database().reference().child("relationship").child(self.protectID!)
        singleUserRealtionship.HelperID = userDataManager.uid
        addRef.setValue([
            "emengecyID" : singleUserRealtionship.emengecyID,
            "helperID" : userDataManager.uid,
            "myID" : singleUserRealtionship.myID
            ])
        let addMeRef = Database.database().reference().child("relationship").child(userDataManager.uid)
        addMeRef.setValue([
            "emengecyID" : protectID,
            "helperID" : userDataManager.helperID,
            "myID" : userDataManager.uid
            ])
        //        performSegue(withIdentifier: "goToMother", sender: nil)
//let vc = self.storyboard!.instantiateViewController(withIdentifier: "setTime") as! AddRelationshipViewControllerFromMessage
                self.navigationController?.popViewController(animated: true)
        //        self.pushViewController(vc, animated: true)
        //self.navigationController?.setViewControllers([vc], animated: true)
    }
    
    func addTapToHideKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(tap:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(tap: UITapGestureRecognizer) {
        //        self.email.endEditing(true)
        //        self.password.endEditing(true)
        self.addEmailLabel.endEditing(true)
    }
}
