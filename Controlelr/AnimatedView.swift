
import UIKit
import QuartzCore

private let baseColor = UIColor(red: 0.0097, green: 0.5678, blue: 0.8347, alpha: 1)

class AutogateAnimatedLogoView: UIView {
    
    // MARK: - Properties
    // MARK: Configure
    var logoSize: CGFloat = 100 {
        didSet {
            invalidateIntrinsicContentSize()
            setNeedsLayout()
        }
    }
    
    /// The array of UIView instances are weakly referenced. The property of `isUserInteractionEnabled` of each view is set to false when they are assigned to this property. When the loadingIndicator is deallocated, The property of `isUserInteractionEnabled` of each view is recovered to their original value.
    ///
    /// Alternatively, you can assign an empty array to this property to this property to get all previous stored views recover the value of `isUserInteractionEnabled`.
    var userInteractionDisabledViews: [UIView] {
        get {
            return _userInteractionDisabledViews.compactMap { $0.value }
        }
        set {
            _userInteractionDisabledViews = newValue.map { WeakContainer($0) }
        }
    }
    
    private var _userInteractionDisabledViews: [WeakContainer<UIView>] = [] {
        didSet {
            // recover the original value for isUserInteractionEnabled
            zip(oldValue, _cachedUserInteractionValue).forEach { (weakContainer, flag) in
                if let view = weakContainer.value, let flag = flag {
                    view.isUserInteractionEnabled = flag
                }
            }
            
            // cache original value of isUserInteractionEnabled
            _cachedUserInteractionValue = _userInteractionDisabledViews.map { $0.value?.isUserInteractionEnabled }
            
            // set them all to false
            _userInteractionDisabledViews.forEach { $0.value?.isUserInteractionEnabled = false }
        }
    }
    
    private var _cachedUserInteractionValue: [Bool?] = []
    
    // MARK: State
    private(set) var isAnimating: Bool = false
    
    // MARK: Layers
    private let mothershipLayer = AnimatedLogoPart1Layer()
    private let satelliteLayer = AnimatedLogoPart2Layer()
    
    // MARK: - Overriding methods
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    convenience init(logoSize: CGFloat) {
        self.init(frame: .zero)
        self.logoSize = logoSize
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: logoSize, height: logoSize)
    }
    
    func commonInit() {
        backgroundColor = .clear
        
        layer.addSublayer(mothershipLayer)
        layer.addSublayer(satelliteLayer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        mothershipLayer.frame = bounds
        
        let radius: CGFloat = 0.5 * logoSize
        let angle: CGFloat = 0.8
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        satelliteLayer.frame.size = CGSize(width: logoSize * 0.0856, height: logoSize * 0.0856)
        satelliteLayer.position = CGPoint(x: center.x+(cos(angle)*radius), y: center.y+(sin(angle)*radius))
    }
    
    // MARK: - Methods
    func startAnimating() {
        guard !isAnimating else {
            return
        }
        
        isAnimating = true
        
        let animation = CABasicAnimation()
        animation.keyPath = "transform.rotation.z"
        animation.isAdditive = true
        animation.fromValue = Float(0)
        animation.toValue = Float.pi * 2
        animation.duration = 2
        animation.repeatCount = Float.infinity
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animation.isRemovedOnCompletion = false
        mothershipLayer.add(animation, forKey: "rotate")
        
        let animation1 = CABasicAnimation()
        animation1.keyPath = "transform.scale"
        animation1.fromValue = 1
        animation1.toValue = 2
        animation1.duration = 1
        
        let animation2 = CABasicAnimation()
        animation2.keyPath = "transform.scale"
        animation2.beginTime = 1
        animation2.fromValue = 2
        animation2.toValue = 1
        animation2.duration = 1
        
        let animationGroup = CAAnimationGroup()
        animationGroup.animations = [animation1, animation2]
        animationGroup.isRemovedOnCompletion = false
        animationGroup.repeatCount = Float.infinity
        animationGroup.duration = 2
        animationGroup.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        satelliteLayer.add(animationGroup, forKey: "scale")
    }
    
    public func stopAnimating() {
        guard isAnimating else {
            return
        }
        isAnimating = false
        [mothershipLayer, satelliteLayer].forEach { $0.removeAllAnimations() }
    }
}

// MARK: - Custom layers
private class AnimatedLogoPart2Layer: CAShapeLayer {
    override init() {
        super.init()
        fillColor = baseColor.cgColor
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    override func layoutSublayers() {
        super.layoutSublayers()
        updatePath()
    }
    
    private func updatePath() {
        let size = min(frame.width, frame.height)
        let path = UIBezierPath(arcCenter: CGPoint(x: bounds.midX, y: bounds.midY),
                                radius: size / 2,
                                startAngle: 0,
                                endAngle: CGFloat.pi * 2,
                                clockwise: true)
        self.path = path.cgPath
    }
}

private class AnimatedLogoPart1Layer: CALayer {
    private let progress: CGFloat = 0
    
    override init() {
        super.init()
        contentsScale = UIScreen.main.scale
        setNeedsDisplay()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    override func draw(in ctx: CGContext) {
        let size = min(frame.width, frame.height)
        
        UIGraphicsPushContext(ctx)
        
        let progressAngle = progress * CGFloat.pi * 2
        
        let firstOrbitColor = UIColor(red: 0.8415, green: 0.9355, blue: 0.9591, alpha: 1).cgColor
        let secondOrbitColor = UIColor(red: 0.4443, green: 0.8204, blue: 0.8973, alpha: 1).cgColor
        let thirdOrbitColor = secondOrbitColor
        
        let innerCircleRadius = 0.2380 * size
        let outerCircleRadius = 0.3572 * size
        
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        ctx.renderCircle(at: center, radius: innerCircleRadius, color: baseColor.cgColor)
        ctx.renderCircleOutline(at: center, radius: outerCircleRadius, color: baseColor.cgColor, strokeWidth: 2)
        
        let firstOrbitPosition = orbitPointFor(angle: -2.4 + progressAngle, radius: outerCircleRadius)
        ctx.renderCircle(at: firstOrbitPosition, radius: 0.0857 * size, color: UIColor.white.cgColor)
        ctx.renderCircle(at: firstOrbitPosition, radius: 0.0571 * size, color: firstOrbitColor)
        
        let secondOrbitPosition = orbitPointFor(angle: 2.7 + progressAngle, radius: outerCircleRadius)
        ctx.renderCircle(at: secondOrbitPosition, radius: 0.0714 * size, color: UIColor.white.cgColor)
        ctx.renderCircle(at: secondOrbitPosition, radius: 0.0429 * size, color: secondOrbitColor)
        
        let thirdOrbitPosition = orbitPointFor(angle: 0.8 + progressAngle, radius: outerCircleRadius)
        ctx.renderCircle(at: thirdOrbitPosition, radius: 0.0714 * size, color: UIColor.white.cgColor)
        ctx.renderCircle(at: thirdOrbitPosition, radius: 0.0429 * size, color: thirdOrbitColor)
        
        UIGraphicsPopContext()
    }
    
    private func orbitPointFor(angle: CGFloat, radius: CGFloat) -> CGPoint {
        let center = CGPoint(x: bounds.width / 2, y: bounds.height / 2)
        return CGPoint(x: center.x+(cos(angle)*radius), y: center.y+(sin(angle)*radius))
    }
}

// MARK: - Helpers
private extension CGContext {
    func renderCircle(at point: CGPoint, radius: CGFloat, color: CGColor) {
        beginPath()
        setFillColor(color)
        addArc(center: point, radius: radius, startAngle: 0, endAngle: CGFloat.pi * 2, clockwise: false)
        closePath()
        fillPath()
    }
    
    func renderCircleOutline(at point: CGPoint, radius: CGFloat, color: CGColor, strokeWidth: CGFloat) {
        setStrokeColor(color)
        setLineWidth(strokeWidth)
        beginPath()
        setStrokeColor(color)
        addArc(center: point, radius: radius, startAngle: 0, endAngle: CGFloat.pi * 2, clockwise: false)
        closePath()
        strokePath()
    }
}

private struct WeakContainer<T: AnyObject> {
    weak var value: T?
    init(_ value: T) {
        self.value = value
    }
}
