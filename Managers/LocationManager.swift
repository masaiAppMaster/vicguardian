//
//  LocationManager.swift
//  VicGuardian
//
//  Created by sai ma on 2/5/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation
import CoreLocation
import Firebase

enum locationDataEvent {
    case shareLocationUpdate
}

class LocationManager: NSObject, CLLocationManagerDelegate {

    public let locationDataEventDispatcher = EventDispatcher<locationDataEvent>()
    var userId: String!
    var locationManager: CLLocationManager
    var currentLocation = CLLocationCoordinate2DMake(-37.876633, 145.044351)
    var sharedLocation = CLLocationCoordinate2DMake(-37.876633, 145.044351)
    var locationRef: DatabaseReference!
    
    

    override init() {
        locationManager = CLLocationManager()
        super.init()
        locationManager.delegate = self
    }
    
    func start(userid: String) {
        self.userId = userid
        locationManager.startUpdatingLocation()

        //        var userRef = Database.database().reference().child("us
        //        userRef.queryOrderedByValue(userId)
        
        
        
        let locationRef = Database.database().reference().child("location").child(userid)
        locationRef.setValue([
            "lat" : currentLocation.latitude,
            "long" : currentLocation.longitude
        ])
        
//        shareLocationRef = Database.database().reference().child("location").child(userDataManager.helperID)
        
        
        //TODO: add location to the location
        //subscribe share location update
        //and post notificaion
    }
    
    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations:
        [CLLocation]) {
        
        guard let location = locations.last else { return }
        currentLocation = location.coordinate
        let locationRef = Database.database().reference().child("location").child(userId)
        locationRef.setValue([
            "lat" : currentLocation.latitude,
            "long" : currentLocation.longitude
            ])
    }
    
    func stopUpdata() {
        locationManager.stopUpdatingLocation()
    }
    
}
