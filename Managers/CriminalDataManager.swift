//
//  CriminalDataManager.swift
//  VicGuardian
//
//  Created by sai ma on 7/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation
import Firebase

class CriminalDataManager {
    
    var array: [CriminalData] = []
    
    init() {
        if let path = Bundle.main.path(forResource: "VicData", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                
                let decoder = JSONDecoder()
                array = try decoder.decode([CriminalData].self, from: data)
            } catch {
            }
        }
    }
}



