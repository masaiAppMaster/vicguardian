//
//  UserDataManager.swift
//  VicGuardian
//
//  Created by sai ma on 2/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation
import Firebase

enum UserDataEvent {
    case helperMessageUpdate
    case emergencyMessageUpdate
    case helperIDUpdate
}


class UserDataManager {
    
    
    public let userDataEventDispatcher = EventDispatcher<UserDataEvent>()

    var uid: String
        
    var relationshipRef: DatabaseReference
    var userRef: DatabaseReference
    var hashelperRef: DatabaseReference
//    var criminalYearRef: DatabaseReference!
//    var helperRef: DatabaseReference
    var users: [User] {
        didSet{
            
        }
    }
    var userName: String = ""
    var phone: String = "000"
    var relationships: [Relationship]
    var emergencyName: String = ""
    var emergencyEmail: String = ""
    var emergencyMessageLastReadID: Double? = nil
    var HelperLastReadID: Double? = nil
    var helperName: String?
    var helperEmial: String?
    var userToken: String?
    var emergencyID: String? {
        didSet{
            
            userName = users.filter { $0.id == uid }.first?.name ?? ""
            phone = users.filter { $0.id == uid }.first?.phone ?? ""
            guard let emergencyID = emergencyID else {
                return
            }
            
            emergencyName = users.filter { $0.id == emergencyID }.first?.name ?? ""
            emergencyEmail = users.filter { $0.id == emergencyID }.first?.email ?? ""
            
            emergencyMessagesMeRef = Database.database().reference().child("message").child(uid).child(emergencyID)

            emergencyMessagesMeRef?.observe(DataEventType.value) { snapShot in
                
                self.emergencyMeMessages = self.getChatModelsFromSnapShot(snapShot: snapShot)
//                self.userDataEventDispatcher.dispatch(UserDataEvent.emergencyMessageUpdate)

                
            }
            
            emergencyMessagesOtherRef = Database.database().reference().child("message").child(emergencyID).child(uid)
            
            emergencyMessagesOtherRef?.observe(DataEventType.value) { snapShot in
                
                self.emergencyOtherMessages = self.getChatModelsFromSnapShot(snapShot: snapShot)
                self.userDataEventDispatcher.dispatch(UserDataEvent.emergencyMessageUpdate)
            }
        }
    }
    
    public func setEmergencyLastReadID() {
        emergencyMessageLastReadID = emergencyOtherMessages.last?.timeStamp
    }
    
    public func setHelperLastReadID() {
        HelperLastReadID = helperOtherMessages.last?.timeStamp
    }
    
    public func persist() {
        UserDefaults.standard.set(HelperLastReadID, forKey: "HelperLastReadID")
        UserDefaults.standard.set(emergencyMessageLastReadID, forKey: "emergencyMessageLastReadID")
        UserDefaults.standard.synchronize()
    }
    
    private func updateEmergencyAllMessages() {
        
    }
    
    private func getChatModelsFromSnapShot(snapShot: DataSnapshot) -> [SDChatModel] {
        var array: [SDChatModel] = []
        let enumerator = snapShot.children
        while let snapShotValue = enumerator.nextObject() as? DataSnapshot {
            let dict = snapShotValue.value as! [String: String]
            let message = SDChatModel()
            message.text = dict["text"] ?? ""
            message.timeStamp = Double(dict["timestamp"]!)!
            message.imageName = dict["image"] ?? ""
            message.isImage
            array.append(message)
        }
        return array
    }
    
    
    var helperID: String?
    {
        didSet{
            guard let helperID = helperID else {
                return
            }
            self.helperName = users.filter { $0.id == helperID }.first?.name ?? ""
            self.helperEmial = users.filter { $0.id == helperID }.first?.email ?? ""
            
            //这些应该都放在user下面
            helpMessagesRef = Database.database().reference().child("message").child(uid).child(helperID)
            
            helpMessagesRef?.observe(DataEventType.value) { snapShot in
                
                self.helperMeMessages = self.getChatModelsFromSnapShot(snapShot: snapShot)
//                self.userDataEventDispatcher.dispatch(UserDataEvent.helperMessageUpdate)
            }
            
            helpMessagesOtherRef = Database.database().reference().child("message").child(helperID).child(uid)
            
            helpMessagesOtherRef?.observe(DataEventType.value) { snapShot in
                
                self.helperOtherMessages = self.getChatModelsFromSnapShot(snapShot: snapShot)
                self.userDataEventDispatcher.dispatch(UserDataEvent.helperMessageUpdate)
            }
            
        }
    }
//    var helpers: [User]
    
    var helpMessages: [SDChatModel] = []
    var emergencyMeMessages: [SDChatModel] = []
    var emergencyOtherMessages: [SDChatModel] = []
    var emergencyAllMeesages:[SDChatModel] = []
    var helperMeMessages: [SDChatModel] = []
    var helperOtherMessages: [SDChatModel] = []
    var helperAllMeesages:[SDChatModel] = []
    
    
    var helpMessagesRef: DatabaseReference?
    var helpMessagesMeRef: DatabaseReference?
    var helpMessagesOtherRef: DatabaseReference?
    var emergencyMessagesMeRef: DatabaseReference?
    var emergencyMessagesOtherRef: DatabaseReference?
    
    
    init(uid: String) {
        self.uid = uid
//        messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
        
        relationshipRef = Database.database().reference().child("relationship").child(uid)
        userRef = Database.database().reference().child("user")
        hashelperRef = Database.database().reference().child("relationship")
//        helperRef = Database.database().reference().child("messages")
        users = []
        relationships = []
//        helpers = []
        starObserve()
        
        self.HelperLastReadID = UserDefaults.standard.double(forKey: "HelperLastReadID")
        self.emergencyMessageLastReadID = UserDefaults.standard.double(forKey: "emergencyMessageLastReadID") as! Double
   
        let fcmToken = UserDefaults.standard.value(forKey: "token")
        Database.database().reference().child("token/\(uid)").setValue(["token": fcmToken])
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        self.userToken = fcmToken
        if uid != "" {
            Database.database().reference().child("token/\(uid)").setValue(["token": fcmToken])
        }
        
    }
    
    func starObserve() {
//        helpers.ob
        
        
        userRef.observe(DataEventType.value) { snapShot in
            let enumerator = snapShot.children
            while let snapShotValue = enumerator.nextObject() as? DataSnapshot {
                let dict = snapShotValue.value as! [String: String]
                let singleUser = User()
                singleUser.email = dict["email"]!
                singleUser.id = dict["id"]!
                singleUser.name = dict["name"]!
                singleUser.phone = dict["phone"] ?? "0432484586"
                self.users.append(singleUser)
            }
            self.listenRealtionshipRef()
        }
        
        

        
        hashelperRef.observe(DataEventType.value) { snapShot in
            let emumerator = snapShot.children
            while let snapShotValue = emumerator.nextObject() as? DataSnapshot {
                let dict = snapShotValue.value as! [String: String]
                let singeRelationship = Relationship()
                singeRelationship.emengecyID = dict["emengecyID"]
                singeRelationship.HelperID = dict["helperID"]
                singeRelationship.myID = dict["myID"]!
                self.relationships.append(singeRelationship)
                
            }
        }
    }
    
    func listenRealtionshipRef() {
        relationshipRef.observe(DataEventType.value) { snapShot in
            let value = snapShot.value as? NSDictionary
            self.emergencyID = value?["emengecyID"] as? String ?? nil
            self.helperID = value?["helperID"] as? String ?? nil
            self.userDataEventDispatcher.dispatch(UserDataEvent.helperIDUpdate)
        }
    }
    
//    func listenCriminaData(year: Int) {
//        criminalYearRef = Database.database().reference().child(String(year))
//
//        criminalYearRef.observe(DataEventType.value) { snapShot in
//            let dict = snapShot as! NSDictionary
//            for (suburb,count) in dict {
//
//
//            }
//        }
//
//    }
 
    var hasEmergency: Bool {
        return emergencyID != nil
    }
    
    var hasHelp: Bool {
        return helperID != nil
    }
    
    func setToken() {
        //check if token existed if yes update to database
    }
}
