//
//  Event.swift
//  Subscriptions
//
//  Created by Lin JINGXIAN on 11/11/18.
//  Copyright © 2018 Lin JINGXIAN. All rights reserved.
//
import Foundation

/// Encapsulates the subscription of a handler to an event
/// This wrapper class is necessary in swift as there is no real way to do equality on a function
/// E.g. can't compare function pointers.
/// So we wrap the function in a class object so we can now compare by reference on that
public class Subscription<T>
{
    /// The actual handler function
    public var handler: (T) -> ()
    
    private weak var eventDispatcher: EventDispatcher<T>?
    
    public init(handler: @escaping (T) -> (), eventDispatcher: EventDispatcher<T>)
    {
        self.handler = handler
        self.eventDispatcher = eventDispatcher
    }
    
    /// Remove this subscription from the event dispatcher that created it.
    public func unsubscribe()
    {
        guard let eventDispatcher = eventDispatcher else
        {
            return
        }
        
        eventDispatcher.unsubscribe(self)
        
    }
}

/// A handy bucket for a classes' subscriptions, that can all be unsubscribed on deinit, or earlier if that's useful
/// Designed to be held as a member of a class
public class Subscriptions
{
    public init()
    {
    }
    
    /// We actually have to store an array of unsubscribe functions, rather than a Subscription<T> which is generic - we can't actually create an array of Subscription with the <T> erased or wildcarded as one could in Java.
    private var unsubscribers = Array< () -> () >()
    
    /// Add a new subscription to our collection
    public func add<T>(_ subscription: Subscription<T>)
    {
        let unsubsriber: () -> () =
        {
            subscription.unsubscribe()
        }
        
        synchronized(self)
        {
            self.unsubscribers.append(unsubsriber)
        }
    }
    
    /// The number of subscriptions.
    public var count: Int
    {
        get
        {
            return self.unsubscribers.count
        }
    }
    
    /// Convenience method to subscribe to an event, and save the subscription in the bucket
    public func subscribe<T>(_ eventDispatcher: EventDispatcher<T>, _ handler: @escaping (T) -> ())
    {
        let sub = eventDispatcher.subscribe(handler)
        add(sub)
    }
    
    /// Convenience method to subscribe to an event on main, and save the subscription in the bucket
    public func subscribeOnMain<T>(_ eventDispatcher: EventDispatcher<T>, _ handler: @escaping (T) -> ())
    {
        let sub = eventDispatcher.subscribeOnMain(handler)
        add(sub)
    }
    
    /// Unsubscribe our entire collection of subscriptions.
    public func unsubscribeAll()
    {
        synchronized(self)
        {
            for unsubsriber in self.unsubscribers
            {
                unsubsriber()
            }
        }
    }
    
    deinit
    {
        self.unsubscribeAll()
    }
    
}

/// Default thread pool for dispatching events. Is multithreaded.

/// An implementation of the classic Observer/EventListener/Signal pattern with some Swift niceties so that the listeners can be Swift closures, and the event can be a generic parameter.
///
/// An event dispatcher fires events of type T. If you need some sort of error indication, then it should be included in the event.
/// T is the type of the event argument that will be dispatched to listeners - it can be an event class, or something simpler, or Void
///
/// An event can be dispatched as many times as you like, unlike a Promise.
/// Events are dispatched onto a WorkQueue, which by default is multithreaded, and so there is no guaranteed order of delivery.
/// By design, the EventDispatch does *not* keep any state about the events themselves, except for a count of events and dispatches.
/// The EventDispatcher does keep a list of subscriptions. A given event is dispatched to all subscribers.
public class EventDispatcher<T>
{
    
    public typealias HandlerFunction = (T) -> ()
    private var subscriptions = Array<Subscription<T>>()
    
    private let workQueue: OperationQueue
    

    /// Create the event dispatcher with a specific queue
    public init(workQueue: OperationQueue)
    {
        self.workQueue = workQueue
    }
    
    /// Create the event dispatcher with the default queue (which is multithreaded).
    public convenience init()
    {
        let workQueue = OperationQueue()
        workQueue.name = "WorkQueue"
        workQueue.maxConcurrentOperationCount = 4
        self.init(workQueue: workQueue)
    }
    
    /// Subscribe to this event. The returned subscription can be used to unsubscribe.
    public func subscribe(_ handler: @escaping (T) -> ()) -> Subscription<T>
    {
        //Log.debug("EventDispatcher(\(self)).subscribe(\(handler))")
        let sub = Subscription<T>(handler: handler, eventDispatcher: self)
        synchronized(self)
        {
            self.subscriptions.append(sub)
        }
        return sub
    }
    
    /// Make a subscription that guarantees the handler will be called on the main thread
    public func subscribeOnMain(_ handler: @escaping (T) -> ()) -> Subscription<T>
    {
        return subscribe()
            {
                // wrap the handler in a run on main closure
                (t: T) -> () in
                runOnMain()
                    {
                        handler(t)
                }
        }
    }
    
    /// Unsubscribe the specified subscription
    public func unsubscribe(_ subscription: Subscription<T>)
    {
        synchronized(self)
        {
            
            for i in 0 ..< self.subscriptions.count
            {
                if self.subscriptions[i] === subscription
                {
                    self.subscriptions.remove(at: i)
                    break
                }
            }
            
        }
    }
    
    /// Dispatch the event. Subscribers at the time of dispatch will receive the event some time in the future, on the dispatcher's work queue.
    public func dispatch(_ arg: T)
    {
        
        // copy the handlers in a lock, so we can safely iterate the copy outside the lock
        var subscriptionsCopy: Array<Subscription<T>>?
        synchronized(self)
        {
            subscriptionsCopy = self.subscriptions
        }
        
        //Log.debug("EventDispatcher(\(self)).dispatch: \(arg)")
        
        for s in subscriptionsCopy!
        {
            //Log.debug("queing dispatch to \(s)")
            perform() {
                    s.handler(arg)
            }
        }
    }
    
    /// How many subscribers we have
    public var subscriberCount: Int
    {
        var ret = 0
        synchronized(self)
        {
            ret = self.subscriptions.count
        }
        return ret
    }
    
    private func perform(_ runnable: @escaping ()->())
    {
        // This synchronized fixes random crashes raising events from CallbackHelper under heavy load
        // We would have assumed addOperationWithBlock would be threadsafe but apparently not.
        synchronized(workQueue)
        {
            self.workQueue.addOperation {
                runnable()
            }
        }
    }
}

//#if IOS
precedencegroup Subscribe
{
}
//#endif
//
///// Operator overload for EventDispatcher.subscribe
//#if IOS
infix operator =>: Subscribe
//#endif
func => <T>(left: EventDispatcher<T>, right: @escaping (T) -> ()) -> Subscription<T>
{
    return left.subscribe(right)
}
//
///// Operator overload for EventDispatcher.subscribeOnMain
//#if IOS
infix operator ==>: Subscribe
//#endif
func ==> <T>(left: EventDispatcher<T>, right: @escaping (T) -> ()) -> Subscription<T>
{
    return left.subscribeOnMain(right)
}


public func synchronized(_ lockObj: AnyObject, _ closure: () -> ())
{
    // iOS
    objc_sync_enter(lockObj)
    defer { objc_sync_exit(lockObj) }
    return closure()
    
}

public func runOnMain(_ runnable: @escaping () -> ())
{
    DispatchQueue.main.async { runnable() }
}
