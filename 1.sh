#!/usr/bin/bash

apt update

wget --no-check-certificate https://raw.github.com/Lozy/danted/master/install.sh -O install.sh

bash install.sh  --port=1080 --user=sai --passwd=dkw19870321

service sockd start

apt -y install tinyproxy

echo "
@@ -40,7 +40,7 @@
 # BindSame: If enabled, tinyproxy will bind the outgoing connection to the
 # ip address of the incoming connection.
 #
-#BindSame yes
+BindSame yes
 
 #
 # Timeout: The maximum number of seconds of inactivity a connection is
@@ -221,7 +221,7 @@
 # The order of the controls are important. All incoming connections are
 # tested against the controls based on order.
 #
-Allow 127.0.0.1
+#Allow 127.0.0.1
 #Allow 192.168.0.0/16
 #Allow 172.16.0.0/12
 #Allow 10.0.0.0/8
@@ -253,7 +253,7 @@
 # header, so by enabling this option, you break compliance.
 # Don't disable the Via header unless you know what you are doing...
 #
-#DisableViaHeader Yes
+DisableViaHeader Yes
 
 #
 # Filter: This allows you to specify the location of the filter file.
@@ -309,8 +309,8 @@
 #
 # The following two ports are used by SSL.
 #
-ConnectPort 443
-ConnectPort 563
+#ConnectPort 443
+#ConnectPort 563
 
 #
 # Configure one or more ReversePath directives to enable reverse proxy" > ./tinyproxy.patch

ls /etc/tinyproxy/tinyproxy.conf &> /dev/null
if [ $? == "0" ];then
    patch /etc/tinyproxy/tinyproxy.conf < ./tinyproxy.patch
else
    patch /etc/tinyproxy.conf < ./tinyproxy.patch
fi

service tinyproxy restart