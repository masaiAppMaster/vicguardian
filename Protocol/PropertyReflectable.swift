//
//  PropertyReflectable.swift
//  VicGuardian
//
//  Created by sai ma on 11/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

//import Foundation
//
//protocol PropertyReflectable {
//    typealias RepresentationType = [String:Any]
//    typealias ValuesType = [Any]
//    typealias NamesType = [String]
//    var propertyDictRepresentation: RepresentationType {get}
//    var propertyValues: ValuesType {get}
//    var propertyNames: NamesType {get}
//    static var propertyCount: Int {get}
//
//    init(_ r:RepresentationType)
//}
//
//extension PropertyReflectable {
//    var propertyDictRepresentation: RepresentationType {
//        var result: [String:Any] = [:]
//        for case let (label, value) in Mirror(reflecting: self).children {
//            guard let l = label else{
//                continue
//            }
//            result.updateValue(value, forKey: l)
//        }
//        return result
//    }
//
//    var propertyValues: ValuesType {
//        return Array(propertyDictRepresentation.values)
//    }
//
//    var propertyName: NamesType {
//        return Array(propertyDictRepresentation.keys)
//    }
//}

import Foundation

protocol PropertyReflectable{
    typealias RepresentationType = [String:Any]
    typealias ValuesType = [Any]
    typealias NamesType = [String]
    var propertyDictRepresentation: RepresentationType {get}
    var propertyValues: ValuesType {get}
    var propertyNames: NamesType {get}
    static var propertyCount: Int {get}
    //construction from representation type
    init(_ r:RepresentationType)
}


//default implementation
extension PropertyReflectable{
    
    var propertyDictRepresentation: RepresentationType {
        var ret: [String:Any] = [:]
        for case let (label, value) in Mirror(reflecting: self).children {
            guard let l = label else{
                continue
            }
            ret.updateValue(value, forKey: l)
        }
        return ret
    }
    
    var propertyValues: ValuesType {
        return Array(propertyDictRepresentation.values)
    }
    
    var propertyNames: NamesType {
        return Array(propertyDictRepresentation.keys)
    }
}
