//
//  Persistable.swift
//  VicGuardian
//
//  Created by sai ma on 11/4/19.
//  Copyright © 2019 sai ma. All rights reserved.
//

import Foundation

import Foundation

protocol Persistable{
    var ud: UserDefaults {get}
    var persistKey : String {get}
    func persist()
    func unpersist()
}
